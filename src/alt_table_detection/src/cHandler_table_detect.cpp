#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "alt_table_detection/cHandler_table_detect.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <tf/transform_broadcaster.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/surface/convex_hull.h>

#include <pcl/surface/concave_hull.h>

#include <pcl/segmentation/extract_clusters.h>

#include <pcl/surface/mls.h>

#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/visualization/range_image_visualizer.h>


typedef pcl::PointXYZ PointType;

float support_size = 0.2f;

cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("kinect2/qhd/points", 2, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);

    //    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_segmented", 1);
    //    ind_pub = nh.advertise<pcl_msgs::PointIndices>("point_indices", 1);
    //    coef_pub = nh.advertise<pcl_msgs::ModelCoefficients>("planar_coef", 1);
}

void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
//    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

   // pcl_conversions::toPCL(input, *cloud_blob);

//    std::cerr << "PointCloud before: " << cloud_blob->width * cloud_blob->height << " data points." << std::endl;

//    // Convert to the templated PointCloud
  //  pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_filtered);




//        // # # Transformation # #
//        Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
//        // Define a translation of 2.5 meters on the x axis.
//        //transform_2.translation() << 0.0, 0.0, -0.6;
//        transform_2.translation() << 0.0, 0.0, 0.0;
//        // The same rotation matrix as before; tetha radians arround Z axis
//        //transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
//        transform_2.rotate (Eigen::AngleAxisf (0.0, Eigen::Vector3f::UnitX())); //20deg
//        // Print the transformation
//        printf ("\nMethod #2: using an Affine3f\n");
//        std::cout << transform_2.matrix() << std::endl;
//        // Executing the transformation
//      pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
//        // You can either apply transform_1 or transform_2; they are the same
//        pcl::transformPointCloud (*cloud_filtered, *transformed_cloud, transform_2);



    pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
    //pcl::PointCloud<PointType>& pointCloud = *transformed_cloud;
    pcl::PointCloud<PointType>& pointCloud = *point_cloud_ptr;
    //pcl::PointCloud<pcl::PointXYZ> pointCloud;
    pcl::fromROSMsg(input,pointCloud);


    // We now want to create a range image from the above point cloud, with a 1deg angular resolution
    float angularResolution = (float) (  1.0f * (M_PI/180.0f));  //   1.0 degree in radians
    float maxAngleWidth     = (float) (360.0f * (M_PI/180.0f));  // 360.0 degree in radians
    float maxAngleHeight    = (float) (180.0f * (M_PI/180.0f));  // 180.0 degree in radians
    Eigen::Affine3f sensorPose = (Eigen::Affine3f)Eigen::Translation3f(0.0f, 0.0f, 0.0f);
    pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
    float noiseLevel=0.00;
    float minRange = 0.0f;
    int borderSize = 1;

    boost::shared_ptr<pcl::RangeImage> range_image_ptr (new pcl::RangeImage);
    pcl::RangeImage& rangeImage = *range_image_ptr;
    rangeImage.createFromPointCloud(pointCloud, angularResolution, maxAngleWidth, maxAngleHeight,
                                    sensorPose, coordinate_frame, noiseLevel, minRange, borderSize);
    pcl::RangeImageBorderExtractor range_image_border_extractor;
    pcl::NarfKeypoint narf_keypoint_detector (&range_image_border_extractor);
    narf_keypoint_detector.setRangeImage (&rangeImage);
    narf_keypoint_detector.getParameters ().support_size = support_size;
    //narf_keypoint_detector.getParameters ().add_points_on_straight_edges = true;
    //narf_keypoint_detector.getParameters ().distance_for_additional_points = 0.5;

    pcl::PointCloud<int> keypoint_indices;
    narf_keypoint_detector.compute (keypoint_indices);
    std::cout << "Found "<<keypoint_indices.points.size ()<<" key points.\n";


//    // # # Transformation # #
//    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
//    // Define a translation of 2.5 meters on the x axis.
//    transform_2.translation() << 0.0, 0.0, -0.7;
//    // The same rotation matrix as before; tetha radians arround Z axis
//    transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
//    // Print the transformation
//    printf ("\nMethod #2: using an Affine3f\n");
//    std::cout << transform_2.matrix() << std::endl;
//    // Executing the transformation
//    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
//    // You can either apply transform_1 or transform_2; they are the same
//    pcl::transformPointCloud (*cloud_filtered, *transformed_cloud, transform_2);

//    // PassThrough filter for z-axis
//    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
//    filter_zrange.setInputCloud(transformed_cloud);
//    filter_zrange.setFilterFieldName("z");
//    filter_zrange.setFilterLimits(0.0, 0.95);
//    filter_zrange.filter(*zrange_filteredCloud);

//    // PassThrough filter for y-axis
//    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
//    filter_yrange.setInputCloud(zrange_filteredCloud);
//    filter_yrange.setFilterFieldName("y");
//    filter_yrange.setFilterLimits(-2.0, 0.7);
//    filter_yrange.filter(*yzrange_filteredCloud);

//    std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points." << std::endl;

//    // getting the hull of the found plane
//    pcl::PointIndices::Ptr cloud_indices (new pcl::PointIndices ());
//    double z_min = 0.0, z_max =0.7;  // points above the plane with maximum height z_max
//    pcl::PointCloud<pcl::PointXYZ>::Ptr hull_points (new pcl::PointCloud<pcl::PointXYZ> ());
//    pcl::ConcaveHull<pcl::PointXYZ> hull;
//    // hull.setDimension (2); // not necessarily needed, but we need to check the dimensionality of the output
//    hull.setInputCloud(cloud_p);
//    hull.setAlpha(0.1);
//    hull.reconstruct (*hull_points);
//    if (hull.getDimension() == 2)
//    {
//        pcl::ExtractPolygonalPrismData<pcl::PointXYZ> prism;
//        prism.setInputCloud(yzrange_filteredCloud);
//        prism.setInputPlanarHull(hull_points);
//        prism.setHeightLimits(z_min, z_max);
//        prism.segment(*cloud_indices);
//    }
//    // extract the indices above the plane from ititial cloud into a separate one
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o (new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::ExtractIndices<pcl::PointXYZ> extract_obj;
//    extract_obj.setInputCloud (yzrange_filteredCloud);
//    extract_obj.setIndices (cloud_indices);
//    extract_obj.setNegative (false);
//    extract_obj.filter (*cloud_o);

//    //to vizualize the computed hull
//    pcl::visualization::CloudViewer viewerplane ("Convex Hull");
//    viewerplane.showCloud(hull_points);
//    while (!viewerplane.wasStopped())
//    {
//        // Do nothing but wait.
//    }

    // # # Visualization # #
    viewer.removeAllPointClouds();
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> range_filteredCloud_color_handler (point_cloud_ptr, 230, 20, 20); // Red
    //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 230, 20, 20);
    //viewer.addPointCloud (range_image_ptr, range_image_color_handler, "rangeImage");
    viewer.addPointCloud (point_cloud_ptr, range_filteredCloud_color_handler, "normalcloud");
    viewer.addCoordinateSystem(1.0,0);
    viewer.setBackgroundColor(0.05, 0.05, 0.05, 0); // Setting background to a dark grey
    //viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "rangeImage");
    //  viewer.setPosition(800, 400); // Setting visualiser window position
//      PointCloudColorHandlerCustom<PointType> point_cloud_color_handler (point_cloud_ptr, 150, 150, 150);
    //viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>( yzrange_filteredCloud, normals, 20, 0.03);
    //viewer.addPointCloudNormals<pcl::PointXYZ, pcl::PointNormal>(cloud_o, smoothedCloud,20, 0.03, "normals");

      pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints_ptr (new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointCloud<pcl::PointXYZ>& keypoints = *keypoints_ptr;
      keypoints.points.resize (keypoint_indices.points.size ());
      for (size_t i=0; i<keypoint_indices.points.size (); ++i)
      {
          keypoints.points[i].getVector3fMap () = rangeImage.points[keypoint_indices.points[i]].getVector3fMap ();

//          pcl::PointXYZ point;
//          point.getVector3fMap() = rangeImage.points[keypoint_indices.points[i]].getVector3fMap ();
//          std::cout << "bla" << point << std::endl;
//          std::cout << keypoints.points[i].x << std::endl;
      }
      pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> keypoints_color_handler (keypoints_ptr, 0, 255, 0);
      viewer.addPointCloud<pcl::PointXYZ> (keypoints_ptr, keypoints_color_handler, "keypoints");
      viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints");
   //std::cout << keypoints.points[1].x << std::endl;
  std::cout << keypoints.points[1] << std::endl;
          // # # Transformation # #
          Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
          // Define a translation of 2.5 meters on the x axis.
          transform_2.translation() << 0.0, 0.0, -0.6;
          // The same rotation matrix as before; tetha radians arround Z axis
          transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
          // Print the transformation
          //printf ("\nMethod #2: using an Affine3f\n");
          std::cout << transform_2.matrix() << std::endl;
          // Executing the transformation
          pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
          // You can either apply transform_1 or transform_2; they are the same
          pcl::transformPointCloud (*point_cloud_ptr, *transformed_cloud, transform_2);

          // # # Transformation # #
          Eigen::Affine3f transform_points = Eigen::Affine3f::Identity();
          // Define a translation of 2.5 meters on the x axis.
          transform_points.translation() << 0.0, 0.0, -0.6;
          // The same rotation matrix as before; tetha radians arround Z axis
          transform_points.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
          // Print the transformation
          //printf ("\nMethod #2: using an Affine3f\n");
          std::cout << transform_points.matrix() << std::endl;
          // Executing the transformation
          pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_points (new pcl::PointCloud<pcl::PointXYZ> ());
          // You can either apply transform_1 or transform_2; they are the same
          pcl::transformPointCloud (*keypoints_ptr, *transformed_points, transform_points);

                pcl::PointCloud<pcl::PointXYZ>& transformedkeypoints = *transformed_points;

                for (int j=0;j<transformedkeypoints.points.size();++j){
                           std::cout << transformedkeypoints.points[j] << endl;
                }
//          std::cout << transformedkeypoints.points[1] << endl;
//          std::cout << transformedkeypoints.points[2] << endl;

          pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> transformed_cloud_color_handler (point_cloud_ptr,0,77,40); // Red
          //viewer.addPointCloud(transformed_cloud, transformed_cloud_color_handler, "transformed_cloud" );

          //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> transformed_points_color_handler (transformed_points, 0,204,255);//blue
         // viewer.addPointCloud (transformed_points, transformed_points_color_handler, "transformed_points");
         // viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "transformed_points");

      // --------------------------
      // -----Show range image-----
      // --------------------------
//      pcl::visualization::RangeImageVisualizer range_image_widget ("Range image");
//      range_image_widget.showRangeImage (rangeImage);


    ros::NodeHandle node;

    tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(0.0, 20.0, 0.0) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "kinect2_link", "new_coord"));
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
//| Help:
//-------
//          p, P   : switch to a point-based representation
//          w, W   : switch to a wireframe-based representation (where available)
//          s, S   : switch to a surface-based representation (where available)

//          j, J   : take a .PNG snapshot of the current window view
//          c, C   : display current camera/window parameters
//          f, F   : fly to point mode

//          e, E   : exit the interactor
//          q, Q   : stop and call VTK's TerminateApp

//           +/-   : increment/decrement overall point size
//     +/- [+ ALT] : zoom in/out

//          g, G   : display scale grid (on/off)
//          u, U   : display lookup table (on/off)

//    r, R [+ ALT] : reset camera [to viewpoint = {0, 0, 0} -> center_{x, y, z}]

//    ALT + s, S   : turn stereo mode on/off
//    ALT + f, F   : switch between maximized window mode and original size

//          l, L           : list all available geometric and color handlers for the current actor map
//    ALT + 0..9 [+ CTRL]  : switch between different geometric handlers (where available)
//          0..9 [+ CTRL]  : switch between different color handlers (where available)

//    SHIFT + left click   : select a point

//          x, X   : toggle rubber band selection mode for left mouse button
