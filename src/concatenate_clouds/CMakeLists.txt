cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(concatenate_clouds)

find_package(catkin REQUIRED COMPONENTS roscpp)
find_package(PCL 1.2 REQUIRED)

catkin_package()

include_directories(${catkin_INCLUDE_DIRS})
include_directories(${PCL_INCLUDE_DIRS})

link_directories(${PCL_LIBRARY_DIRS})

add_definitions(${PCL_DEFINITIONS})

add_executable (concatenate_clouds src/concatenate_clouds.cpp)

target_link_libraries (concatenate_clouds ${PCL_LIBRARIES})

