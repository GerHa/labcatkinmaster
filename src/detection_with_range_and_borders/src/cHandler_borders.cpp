#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "detection_with_range_and_borders/cHandler_borders.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <tf/transform_broadcaster.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>

#include <pcl/surface/convex_hull.h>
#include <pcl/surface/concave_hull.h>

#include <pcl/segmentation/extract_clusters.h>

#include <pcl/features/integral_image_normal.h>

#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>

#include <pcl/surface/mls.h>

#include <pcl/segmentation/region_growing.h>

#include <pcl/range_image/range_image.h>

#include <sensor_msgs/PointCloud2.h>

#include <pcl/features/range_image_border_extractor.h>

typedef pcl::PointXYZ PointType;

cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("kinect2/sd/points", 2, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);

    //    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_segmented", 1);
    //    ind_pub = nh.advertise<pcl_msgs::PointIndices>("point_indices", 1);
    //    coef_pub = nh.advertise<pcl_msgs::ModelCoefficients>("planar_coef", 1);
}


void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
//    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);



    pcl::PointCloud<pcl::PointXYZ> pointCloud;
    pcl::fromROSMsg(input, pointCloud);
    //pcl_conversions::toPCL(input, pointCloud);


    //std::cerr << "PointCloud before: " << cloud_blob->width * cloud_blob->height << " data points." << std::endl;

    // // Convert to the templated PointCloud
    //pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_filtered);


    // We now want to create a range image from the above point cloud, with a 1deg angular resolution
    float angularResolution = (float) (  1.0f * (M_PI/180.0f));  //   1.0 degree in radians
    float maxAngleWidth     = (float) (360.0f * (M_PI/180.0f));  // 360.0 degree in radians
    float maxAngleHeight    = (float) (180.0f * (M_PI/180.0f));  // 180.0 degree in radians
    Eigen::Affine3f sensorPose = (Eigen::Affine3f)Eigen::Translation3f(0.0f, 0.0f, 0.0f);
    pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
    float noiseLevel=0.00;
    float minRange = 0.0f;
    int borderSize = 1;

    boost::shared_ptr<pcl::RangeImage> range_image_ptr (new pcl::RangeImage);
    pcl::RangeImage& rangeImage = *range_image_ptr;
    rangeImage.createFromPointCloud(pointCloud, angularResolution, maxAngleWidth, maxAngleHeight,
                                    sensorPose, coordinate_frame, noiseLevel, minRange, borderSize);

    //rangeImage.integrateFarRanges (far_ranges);

    //viewer.setBackgroundColor (1, 1, 1);
    //viewer.addCoordinateSystem (1.0f, "global");
    //pcl::visualization::PointCloudColorHandlerCustom<PointType> point_cloud_color_handler (range_image_ptr, 0, 0, 0);
    //viewer.addPointCloud (range_image_ptr, point_cloud_color_handler, "original point cloud");
    //PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 150, 150, 150);
    //viewer.addPointCloud (range_image_ptr, range_image_color_handler, "range image");
    //viewer.setPointCloudRenderingProperties (PCL_VISUALIZER_POINT_SIZE, 2, "range image");

    // -------------------------
    // -----Extract borders-----
    // -------------------------
    pcl::RangeImageBorderExtractor border_extractor (&rangeImage);
    pcl::PointCloud<pcl::BorderDescription> border_descriptions;
    border_extractor.compute (border_descriptions);

    pcl::PointCloud<pcl::PointWithRange>::Ptr border_points_ptr(new pcl::PointCloud<pcl::PointWithRange>);
    pcl::PointCloud<pcl::PointWithRange>::Ptr veil_points_ptr(new pcl::PointCloud<pcl::PointWithRange>);
    pcl::PointCloud<pcl::PointWithRange>::Ptr shadow_points_ptr(new pcl::PointCloud<pcl::PointWithRange>);
    pcl::PointCloud<pcl::PointWithRange>& border_points = *border_points_ptr,
            & veil_points = * veil_points_ptr,
            & shadow_points = *shadow_points_ptr;
    for (int y=0; y< (int)rangeImage.height; ++y)
    {
        for (int x=0; x< (int)rangeImage.width; ++x)
        {
            if (border_descriptions.points[y*rangeImage.width + x].traits[pcl::BORDER_TRAIT__OBSTACLE_BORDER])
                border_points.points.push_back (rangeImage.points[y*rangeImage.width + x]);
            if (border_descriptions.points[y*rangeImage.width + x].traits[pcl::BORDER_TRAIT__VEIL_POINT])
                veil_points.points.push_back (rangeImage.points[y*rangeImage.width + x]);
            if (border_descriptions.points[y*rangeImage.width + x].traits[pcl::BORDER_TRAIT__SHADOW_BORDER])
                shadow_points.points.push_back (rangeImage.points[y*rangeImage.width + x]);
        }
    }
    viewer.removeAllPointClouds();
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> border_points_color_handler (border_points_ptr, 0, 255, 0);
    viewer.addPointCloud<pcl::PointWithRange> (border_points_ptr, border_points_color_handler, "border points");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "border points");
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> veil_points_color_handler (veil_points_ptr, 255, 0, 0);
    viewer.addPointCloud<pcl::PointWithRange> (veil_points_ptr, veil_points_color_handler, "veil points");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "veil points");
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> shadow_points_color_handler (shadow_points_ptr, 0, 255, 255);
    viewer.addPointCloud<pcl::PointWithRange> (shadow_points_ptr, shadow_points_color_handler, "shadow points");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "shadow points");
    viewer.addCoordinateSystem(1.0,0);

    pcl::PCDWriter writer;
    writer.write("range_image.pcd", rangeImage);
    std::cout << rangeImage << "\n";






//    // # # Transformation # #
//    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
//    // Define a translation of 2.5 meters on the x axis.
//    transform_2.translation() << 0.0, 0.0, -0.7;
//    // The same rotation matrix as before; tetha radians arround Z axis
//    transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
//    // Print the transformation
//    printf ("\nMethod #2: using an Affine3f\n");
//    std::cout << transform_2.matrix() << std::endl;
//    // Executing the transformation
//    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
//    // You can either apply transform_1 or transform_2; they are the same
//    pcl::transformPointCloud (*cloud_filtered, *transformed_cloud, transform_2);

//    // PassThrough filter for z-axis
//    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
//    filter_zrange.setInputCloud(transformed_cloud);
//    filter_zrange.setFilterFieldName("z");
//    filter_zrange.setFilterLimits(0.0, 0.95);
//    filter_zrange.filter(*zrange_filteredCloud);

//    // PassThrough filter for y-axis
//    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
//    filter_yrange.setInputCloud(zrange_filteredCloud);
//    filter_yrange.setFilterFieldName("y");
//    filter_yrange.setFilterLimits(-2.0, 0.7);
//    filter_yrange.filter(*yzrange_filteredCloud);

//   std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points." << std::endl;


//double start = clock();
//pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
//ne.setInputCloud(cloud_o);
//pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
//  // Use all neighbors in a sphere of radius 3cm
//ne.setRadiusSearch(0.03);
//ne.compute(*cloud_normals);
//double finish =clock();

//double total_time = (double)(finish - start)/CLOCKS_PER_SEC;
//cout << "normal estimation TIME=" <<total_time << endl;




    // # # Visualization # #
    //viewer.removeAllPointClouds();
   // pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> range_filteredCloud_color_handler (cloud_o, 230, 20, 20); // Red
    //viewer.addPointCloud (cloud_o, range_filteredCloud_color_handler, "range_filteredCloud");
    // Display one normal out of 20, as a line of length 3cm.
   // viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(cloud_o, normals, 20, 0.03, "normals");
   // viewer.addCoordinateSystem(1.0,0);
    //viewer.setBackgroundColor(0.05, 0.05, 0.05, 0); // Setting background to a dark grey
//    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "original_cloud");
    //viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "range_filteredCloud");
    //    //  viewer.setPosition(800, 400); // Setting visualiser window position

    ros::NodeHandle node;

    tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(0.0, 20.0, 0.0) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "kinect2_link", "new_coord"));
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
//| Help:
//-------
//          p, P   : switch to a point-based representation
//          w, W   : switch to a wireframe-based representation (where available)
//          s, S   : switch to a surface-based representation (where available)

//          j, J   : take a .PNG snapshot of the current window view
//          c, C   : display current camera/window parameters
//          f, F   : fly to point mode

//          e, E   : exit the interactor
//          q, Q   : stop and call VTK's TerminateApp

//           +/-   : increment/decrement overall point size
//     +/- [+ ALT] : zoom in/out

//          g, G   : display scale grid (on/off)
//          u, U   : display lookup table (on/off)

//    r, R [+ ALT] : reset camera [to viewpoint = {0, 0, 0} -> center_{x, y, z}]

//    ALT + s, S   : turn stereo mode on/off
//    ALT + f, F   : switch between maximized window mode and original size

//          l, L           : list all available geometric and color handlers for the current actor map
//    ALT + 0..9 [+ CTRL]  : switch between different geometric handlers (where available)
//          0..9 [+ CTRL]  : switch between different color handlers (where available)

//    SHIFT + left click   : select a point

//          x, X   : toggle rubber band selection mode for left mouse button
