#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "voxelfilter3/cHandler3.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>

cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("pcl_output", 10, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);
    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_segmented", 1);

    ind_pub = nh.advertise<pcl_msgs::PointIndices>("point_indices", 1);
    coef_pub = nh.advertise<pcl_msgs::ModelCoefficients>("planar_coef", 1);
}


void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::PointCloud<pcl::PointXYZ> cloud_filtered;

    pcl::fromROSMsg(input, cloud);

    //std::vector<int> indices;
    //pcl::removeNaNFromPointCloud(cloud,cloud, indices);
    //qDebug() << "not connected";

    //std::cerr << "PointCloud before filtering: " << cloud.width * cloud.height << " data points." << std::endl;


    //Filtering
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> statFilter;
    statFilter.setInputCloud(cloud.makeShared());
    statFilter.setMeanK(10);
    statFilter.setStddevMulThresh(0.2);
    statFilter.filter(cloud_filtered);

    //std::cerr << "PointCloud before filtering: " << cloud_filtered.width * cloud_filtered.height << " data points." << std::endl;

    //Downsampling
    pcl::PointCloud<pcl::PointXYZ> cloud_downsampled;
    pcl::VoxelGrid<pcl::PointXYZ> voxelSampler;
    voxelSampler.setInputCloud(cloud_filtered.makeShared());
    voxelSampler.setLeafSize(0.01f, 0.01f, 0.01f);
    voxelSampler.filter(cloud_downsampled);

    //Segmentation
    pcl::PointCloud<pcl::PointXYZ> cloud_segmented;

    pcl::ModelCoefficients coefficients;
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());

    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> segmentation;
    // Optional
    //segmentation.setOptimizeCoefficients (true);
    segmentation.setModelType(pcl::SACMODEL_PLANE);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setMaxIterations(1000);
    segmentation.setDistanceThreshold(0.01);
    segmentation.setInputCloud(cloud_downsampled.makeShared());
    segmentation.segment(*inliers, coefficients);

    // Publish the model coefficients
    pcl_msgs::ModelCoefficients ros_coefficients;
    pcl_conversions::fromPCL(coefficients, ros_coefficients);
    //to link to the original pc_downsampled better?
    ros_coefficients.header.stamp = input.header.stamp;
    coef_pub.publish(ros_coefficients);

    // Publish the Point Indices
    pcl_msgs::PointIndices ros_inliers;
    pcl_conversions::fromPCL(*inliers, ros_inliers);
    ros_inliers.header.stamp = input.header.stamp;
    ind_pub.publish(ros_inliers);

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(cloud_downsampled.makeShared());
    extract.setIndices(inliers);
    extract.setNegative(true);
    extract.filter(cloud_segmented);

    //Publish the new cloud
    sensor_msgs::PointCloud2 output;
    pcl::toROSMsg(cloud_segmented, output);
    pcl_pub.publish(output);

    viewer.showCloud(cloud_segmented.makeShared());
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
