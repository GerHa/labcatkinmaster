#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "voxelfilter4/cHandler4.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>

cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("pcl_output", 10, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);

    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_segmented", 1);
    ind_pub = nh.advertise<pcl_msgs::PointIndices>("point_indices", 1);
    coef_pub = nh.advertise<pcl_msgs::ModelCoefficients>("planar_coef", 1);
}


void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2), cloud_filtered_blob (new pcl::PCLPointCloud2), cloud_filtered_outl (new pcl::PCLPointCloud2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

    // Fill in the cloud data
    pcl::PCDReader reader;
    reader.read ("/home/gerry/Masterarbeit/Testfotos/Buero/qhdcloud/1453802133461341.pcd", *cloud_blob);
    // /home/gerry/catkin_ws/table_scene_lms400.pcd

    std::cerr << "PointCloud before filtering: " << cloud_blob->width * cloud_blob->height << " data points." << std::endl;

    //Filtering
    pcl::StatisticalOutlierRemoval<pcl::PCLPointCloud2> statFilter;
    statFilter.setInputCloud(cloud_blob);
    statFilter.setMeanK(50);
    statFilter.setStddevMulThresh(1.0);
    statFilter.filter(*cloud_filtered_outl);


  /*For writing the cloud with just statistical outliers removed
    // Convert to the templated PointCloud
    pcl::fromPCLPointCloud2 (*cloud_filtered_outl, *cloud_filtered);
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("/home/gerry/Bilder/stat_outliers_removed.pcd", *cloud_filtered, false);
    */
    pcl::fromPCLPointCloud2 (*cloud_filtered_outl, *cloud_filtered);
    pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("/home/gerry/Bilder/stat_outliers_removed.pcd", *cloud_filtered, false);

    //Downsampling
   // pcl::PointCloud<pcl::PointXYZ> cloud_downsampled;
    pcl::VoxelGrid<pcl::PCLPointCloud2> voxelSampler;
    voxelSampler.setInputCloud(cloud_filtered_outl);
    voxelSampler.setLeafSize(0.01f, 0.01f, 0.01f);  //1cm
    voxelSampler.filter(*cloud_filtered_blob);

    // Convert to the templated PointCloud
    pcl::fromPCLPointCloud2 (*cloud_filtered_blob, *cloud_filtered);

    std::cerr << "PointCloud after filtering: " << cloud_filtered->width * cloud_filtered->height << " data points." << std::endl;

    // Write the downsampled version to disk
    // pcl::PCDWriter writer;
    writer.write<pcl::PointXYZ> ("/home/gerry/Bilder/downsampled.pcd", *cloud_filtered, false);
    // ../../../../../table_scene_lms400_downsampled.pcd


    //SEGMENTATION
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);
    //determines how close a point must be to the model in order to be considered an inlier
    seg.setDistanceThreshold (0.01);

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    int i = 0, nr_points = (int) cloud_filtered->points.size ();
    // While 30% of the original cloud is still there
    while (cloud_filtered->points.size () > 0.3 * nr_points)
    {
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0)
        {
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }

        // Extract the inliers
        extract.setInputCloud (cloud_filtered);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);
        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;


        // Write ervery plane to a seperate pcd file
        std::stringstream ss;

        ss << "../../../../../plane" << i << ".pcd";

       // ss << "table_scene_lms400_plane_" << i << ".pcd";
        writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_f);
        cloud_filtered.swap (cloud_f);
        i++;
     // writer.write("../../../../../filtered_and_downsampled_cloud.pcd", *cloud_filtered);
    }

    viewer.showCloud(cloud_filtered);
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
//| Help:
//-------
//          p, P   : switch to a point-based representation
//          w, W   : switch to a wireframe-based representation (where available)
//          s, S   : switch to a surface-based representation (where available)

//          j, J   : take a .PNG snapshot of the current window view
//          c, C   : display current camera/window parameters
//          f, F   : fly to point mode

//          e, E   : exit the interactor
//          q, Q   : stop and call VTK's TerminateApp

//           +/-   : increment/decrement overall point size
//     +/- [+ ALT] : zoom in/out

//          g, G   : display scale grid (on/off)
//          u, U   : display lookup table (on/off)

//    r, R [+ ALT] : reset camera [to viewpoint = {0, 0, 0} -> center_{x, y, z}]

//    ALT + s, S   : turn stereo mode on/off
//    ALT + f, F   : switch between maximized window mode and original size

//          l, L           : list all available geometric and color handlers for the current actor map
//    ALT + 0..9 [+ CTRL]  : switch between different geometric handlers (where available)
//          0..9 [+ CTRL]  : switch between different color handlers (where available)

//    SHIFT + left click   : select a point

//          x, X   : toggle rubber band selection mode for left mouse button
