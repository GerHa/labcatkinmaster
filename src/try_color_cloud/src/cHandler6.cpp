#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "try_color_cloud/cHandler6.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>

cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    //output_sub = nh.subscribe("pcl_downsampled", 10, &cloudHandler::outputCB, this);
    downsampled_sub = nh.subscribe("pcl_output", 1, &cloudHandler::downsampledCB, this);
    downsampled_pub =nh.advertise<sensor_msgs::PointCloud2>("pcl_downsampled", 1);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);


    //viewer.createViewPort(0.0, 0.0, 0.5, 1.0, output_view);
    viewer.setBackgroundColor(0, 0, 0);

//    viewer.createViewPort(0.5, 0.0, 1.0, 1.0, downsampled_view);
//    viewer.setBackgroundColor(0, 0, 0, downsampled_view);

    viewer.addCoordinateSystem(5.0);
    viewer.initCameraParameters();

}

//void cloudHandler::outputCB(const sensor_msgs::PointCloud2ConstPtr& input)
//{
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
//    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);


//    pcl::fromROSMsg(*input, *cloud_filtered);

//    //SEGMENTATION
//    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
//    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
//    // Create the segmentation object
//    pcl::SACSegmentation<pcl::PointXYZ> seg;
//    // Optional
//    seg.setOptimizeCoefficients (true);
//    // Mandatory
//    seg.setModelType (pcl::SACMODEL_PLANE);
//    seg.setMethodType (pcl::SAC_RANSAC);
//    seg.setMaxIterations (1000);
//    seg.setDistanceThreshold (0.01);

//    // Create the filtering object
//    pcl::ExtractIndices<pcl::PointXYZ> extract;

//    int i = 0, nr_points = (int) cloud_filtered->points.size ();
//    // While 30% of the original cloud is still there
//    while (cloud_filtered->points.size () > 0.1 * nr_points)
//    {
//        // Segment the largest planar component from the remaining cloud
//        seg.setInputCloud (cloud_filtered);
//        seg.segment (*inliers, *coefficients);
//        if (inliers->indices.size () == 0)
//        {
//            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
//            break;
//        }

//        // Extract the inliers
//        extract.setInputCloud (cloud_filtered);
//        extract.setIndices (inliers);
//        extract.setNegative (false);
//        extract.filter (*cloud_p);
//        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

//       std::stringstream ss;
//        ss << "table_scene_lms400_plane_" << i << ".pcd";
//       writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

//        // Create the filtering object
//        extract.setNegative (true);
//        extract.filter (*cloud_f);
//        cloud_filtered.swap (cloud_f);
//        i++;
//    }


//    viewer.removeAllPointClouds(output_view);
//    viewer.addPointCloud<pcl::PointXYZ>(cloud_filtered, "output", output_view);
//}

void cloudHandler::downsampledCB(const sensor_msgs::PointCloud2ConstPtr& input)
{
    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2), cloud_filtered_blob (new pcl::PCLPointCloud2);
    pcl::PCLPointCloud2::Ptr cloud_filtered_outl (new pcl::PCLPointCloud2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

    // Fill in the cloud data
    pcl::PCDReader reader;
   // reader.read ("/home/gerry/catkin_ws/src/extracting_indices/table_scene_lms400.pcd", *cloud_blob);
    reader.read("/home/gerry/Bilder/1447065094.309736562.pcd", *cloud_blob);

    //Filtering
    pcl::StatisticalOutlierRemoval<pcl::PCLPointCloud2> statFilter;
    statFilter.setInputCloud(cloud_blob);
    statFilter.setMeanK(10);
    statFilter.setStddevMulThresh(0.2);
    statFilter.filter(*cloud_filtered_outl);


    //DOWNSAMPLING
    // Create the filtering object: downsample the dataset using a leaf size of 1cm
    pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
    sor.setInputCloud (cloud_filtered_outl);
    sor.setLeafSize (0.02f, 0.02f, 0.02f);
    sor.filter (*cloud_filtered_blob);


    // //////////////////////////////////////////////////////////////////////77
    // ////////////////////////////////////////////////////////////////////////
    pcl::PointCloud<pcl::PointXYZ>::Ptr  cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

    // Convert to the templated PointCloud
    pcl::fromPCLPointCloud2 (*cloud_filtered_blob, *cloud_filtered);

    //SEGMENTATION
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);
    seg.setDistanceThreshold (0.01);

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    int i = 0, nr_points = (int) cloud_filtered->points.size ();
    // While 30% of the original cloud is still there
    while (cloud_filtered->points.size () > 0.1 * nr_points)
    {
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0)
        {
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        // Extract the inliers
        extract.setInputCloud (cloud_filtered);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);
        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

//        std::stringstream ss;
//        ss << "table_scene_lms400_plane_" << i << ".pcd";
//        writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_f);
        cloud_filtered.swap (cloud_f);
        i++;
    }
    viewer.removeAllPointClouds();
    viewer.addPointCloud<pcl::PointXYZ>(cloud_filtered, "output");
    // //////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////

//    sensor_msgs::PointCloud2 output;
//   pcl::toROSMsg(*cloud_filtered, output);
//   downsampled_pub.publish(output);

//    viewer.removeAllPointClouds(downsampled_view);
//    viewer.addPointCloud<pcl::PointXYZ>(cloud_filtered, "downsampled", downsampled_view);

}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    viewer.spinOnce();

    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}


//| Help:
//-------
//          p, P   : switch to a point-based representation
//          w, W   : switch to a wireframe-based representation (where available)
//          s, S   : switch to a surface-based representation (where available)

//          j, J   : take a .PNG snapshot of the current window view
//          c, C   : display current camera/window parameters
//          f, F   : fly to point mode

//          e, E   : exit the interactor
//          q, Q   : stop and call VTK's TerminateApp

//           +/-   : increment/decrement overall point size
//     +/- [+ ALT] : zoom in/out

//          g, G   : display scale grid (on/off)
//          u, U   : display lookup table (on/off)

//    r, R [+ ALT] : reset camera [to viewpoint = {0, 0, 0} -> center_{x, y, z}]

//    ALT + s, S   : turn stereo mode on/off
//    ALT + f, F   : switch between maximized window mode and original size

//          l, L           : list all available geometric and color handlers for the current actor map
//    ALT + 0..9 [+ CTRL]  : switch between different geometric handlers (where available)
//          0..9 [+ CTRL]  : switch between different color handlers (where available)

//    SHIFT + left click   : select a point

//          x, X   : toggle rubber band selection mode for left mouse button
