#include <ros/ros.h>
#include "voxelfilter/loadpc.h"

main (int argc, char **argv)
{
    ros::init (argc, argv, "pcl_read");
    loadpc();

    return 0;
}
