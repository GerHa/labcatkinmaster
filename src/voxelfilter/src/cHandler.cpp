#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>
#include "../include/voxelfilter/cHandler.h"
#include <pcl/filters/statistical_outlier_removal.h>


cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("pcl_output", 10, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);
    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_filtered", 1);
}

void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::PointCloud<pcl::PointXYZ> cloud_filtered;
    sensor_msgs::PointCloud2 output;

    pcl::fromROSMsg(input, cloud);



    //std::vector<int> indices;
    //pcl::removeNaNFromPointCloud(cloud,cloud, indices);
    //qDebug() << "not connected";

    //std::cerr << "PointCloud before filtering: " << cloud.width * cloud.height << " data points." << std::endl;

    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> statFilter;
    statFilter.setInputCloud(cloud.makeShared());
    statFilter.setMeanK(10);
    statFilter.setStddevMulThresh(0.2);
    statFilter.filter(cloud_filtered);

    //std::cerr << "PointCloud before filtering: " << cloud_filtered.width * cloud_filtered.height << " data points." << std::endl;
    pcl::toROSMsg(cloud_filtered, output);
    pcl_pub.publish(output);

    viewer.showCloud(cloud_filtered.makeShared());
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
