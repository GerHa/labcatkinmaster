
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>

class  cloudHandler
{
public:
    cloudHandler();
    void convertPC2toPxyz (const sensor_msgs::PointCloud2 &input, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered);
    void transformStartCloud (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered, pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud);
    void passThroughZ (pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud);
    void passThroughY (pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud);
    void planeSegmentation (pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,     pcl::ModelCoefficients::Ptr coefficients,  pcl::PointIndices::Ptr inliers,  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f);

    void normalEstimationAndClustering (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,  pcl::PointCloud<pcl::Normal>::Ptr normals, std::vector <pcl::PointIndices> &clusters);

    void computeClusterCentroid (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, Eigen::Vector4f &cluster_centroid);
    void computeBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, Eigen::Vector4f &cluster_centroid, Eigen::Vector3f &bboxTransform);
    void cloudCB(const sensor_msgs::PointCloud2 &input);
    void cloudCB2(const sensor_msgs::PointCloud2 &input);
    void timerCB(const ros::TimerEvent&);

protected:
    ros::NodeHandle nh;
    ros::Subscriber pcl_sub;
    ros::Publisher pcl_pub, ind_pub, coef_pub;
    pcl::visualization::PCLVisualizer viewer; //CloudViewer viewer;
    ros::Timer viewer_timer;

};
