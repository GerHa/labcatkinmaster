#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

int
main (int argc, char** argv)
{

  // creates a PointCloud<PointXYZ> boost shared pointer and initializes it
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);


  // loads the PointCloud data from disk into the binary blob


  /*
  if (pcl::io::loadPCDFile<pcl::PointXYZ> ("test_pcd.pcd", *cloud) == -1)
  { */


  // Alternatively, you can read a PCLPointCloud2 blob (available only in PCL 1.x).
  //Due to the dynamic nature of point clouds, we prefer to read them as binary blobs,
  //and then convert to the actual representation that we want to use.
  // reads and converts the binary blob into the templated PointCloud format,
  //here using pcl::PointXYZ as the underlying point type.
  pcl::PCLPointCloud2 cloud_blob;
 if (pcl::io::loadPCDFile ("/home/gerry/Bilder/1447065094.068465219.pcd", cloud_blob) ==-1)
 {

    PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
    return (-1);
  }

 pcl::fromPCLPointCloud2 (cloud_blob, *cloud); //* convert from pcl/PCLPointCloud2 to pcl::PointCloud<T>




 // how the data that was loaded from file
  std::cout << "Loaded "
            << cloud->width * cloud->height
            << " data points from test_pcd.pcd with the following fields: "
            << std::endl;
  for (size_t i = 0; i < cloud->points.size (); ++i)
    std::cout << "    " << cloud->points[i].x
              << " "    << cloud->points[i].y
              << " "    << cloud->points[i].z
              << " "    << cloud->points[i].r
              << " "    << cloud->points[i].g
              << " "    << cloud->points[i].b  << std::endl;

  return (0);
}
