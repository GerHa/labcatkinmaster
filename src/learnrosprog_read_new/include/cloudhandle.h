
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>

class  cloudHandler
{
public:
    cloudHandler();

    void cloudCB(const sensor_msgs::PointCloud2 &input);

    void timerCB(const ros::TimerEvent&);

protected:
    ros::NodeHandle nh;
    ros::Subscriber pcl_sub;
    pcl::visualization::CloudViewer viewer;
    ros::Timer viewer_timer;
};
