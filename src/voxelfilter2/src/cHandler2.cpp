#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>
#include "voxelfilter2/cHandler2.h"
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>


cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("pcl_output", 10, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);
    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_filtered_and_downsampled", 1);
}


void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    pcl::PointCloud<pcl::PointXYZRGB> cloud_filtered;
    sensor_msgs::PointCloud2 output;

    pcl::fromROSMsg(input, cloud);

    //std::vector<int> indices;
    //pcl::removeNaNFromPointCloud(cloud,cloud, indices);
    //qDebug() << "not connected";

    //std::cerr << "PointCloud before filtering: " << cloud.width * cloud.height << " data points." << std::endl;

    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> statFilter;
    statFilter.setInputCloud(cloud.makeShared());
    statFilter.setMeanK(10);
    statFilter.setStddevMulThresh(0.2);
    statFilter.filter(cloud_filtered);

    //std::cerr << "PointCloud before filtering: " << cloud_filtered.width * cloud_filtered.height << " data points." << std::endl;


    pcl::PointCloud<pcl::PointXYZRGB> cloud_downsampled;
    pcl::VoxelGrid<pcl::PointXYZRGB> voxelSampler;
    voxelSampler.setInputCloud(cloud_filtered.makeShared());
    voxelSampler.setLeafSize(0.01f, 0.01f, 0.01f);
    voxelSampler.filter(cloud_downsampled);
    pcl::toROSMsg(cloud_downsampled, output);
    pcl_pub.publish(output);
    viewer.showCloud(cloud_downsampled.makeShared());


}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
