#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include "voxelfilter_topic/cHandler_topic.h"
#include "voxelfilter_topic/loadpc_topic.h"
#include <pcl/io/pcd_io.h>


void loadpc(){

    ros::NodeHandle nh;
    ros::Publisher pcl_pub = nh.advertise<sensor_msgs::PointCloud2> ("pcl_output", 1);

    sensor_msgs::PointCloud2 output;
    pcl::PointCloud<pcl::PointXYZRGBA> cloud;

//    pcl::io::loadPCDFile ("/home/gerry/catkin_ws/table_scene_lms400.pcd", cloud);

    pcl::toROSMsg(cloud, output);
    output.header.frame_id = "odom";

    cloudHandler handler;
    ros::Rate loop_rate(1);
    while (ros::ok())
    {
//        pcl_pub.publish(output);
        ros::spinOnce();
        loop_rate.sleep();
        handler;
    }

}

//wenn loadfile auskommentiert ist
//header:
//  seq: 8
//  stamp:
//    secs: 0
//    nsecs: 0
//  frame_id: odom
//height: 1
//width: 0
//fields:
//  -
//    name: x
//    offset: 0
//    datatype: 7
//    count: 1
//  -
//    name: y
//    offset: 4
//    datatype: 7
//    count: 1
//  -
//    name: z
//    offset: 8
//    datatype: 7
//    count: 1
//is_bigendian: False
//point_step: 16
//row_step: 0
//data: []
//is_dense: True
//---
