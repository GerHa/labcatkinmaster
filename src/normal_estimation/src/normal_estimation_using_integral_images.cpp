     #include <pcl/io/io.h>
     #include <pcl/io/pcd_io.h>
     #include <pcl/features/integral_image_normal.h>
     #include <pcl/visualization/cloud_viewer.h>

     int
     main ()
     {
             // load point cloud from file
             pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
             pcl::io::loadPCDFile ("/home/gerry/Bilder/1447065094.068465219.pcd", *cloud);

             // estimate normals
             // create an object for the normal estimation and compute the normals
             pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);

             pcl::IntegralImageNormalEstimation<pcl::PointXYZ, pcl::Normal> ne;

             /* the following normal estimation methods are available:
              * COVARIANCE_MATRIX,; creates 9 integral images to compute the normal for a specific point
              * from the covariance matrix of its local neighborhoo
              * AVERAGE_3D_GRADIENT,;creates 6 integral images to compute smoothed versions of horizontal
              * and vertical 3D gradients and computes the normals using the cross-product between these two gradients
              * AVERAGE_DEPTH_CHANGE;creates only a single integral image and computes the normals from the average
              * depth changes
              */
             ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
             ne.setMaxDepthChangeFactor(0.02f);
             ne.setNormalSmoothingSize(10.0f);
             ne.setInputCloud(cloud);
             ne.compute(*normals);

             // visualize the point cloud and the corresponding normals
             pcl::visualization::PCLVisualizer viewer("PCL Viewer");
             viewer.setBackgroundColor (0.0, 0.0, 0.0);
             viewer.addPointCloudNormals<pcl::PointXYZ,pcl::Normal>(cloud, normals);

             while (!viewer.wasStopped ())
             {
               viewer.spinOnce ();
             }
             return 0;
     }
