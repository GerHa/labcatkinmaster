#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "rotate_and_range/cHandler_rotate.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <tf/transform_broadcaster.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/surface/convex_hull.h>

#include <pcl/surface/concave_hull.h>

#include <pcl/segmentation/extract_clusters.h>

#include <pcl/surface/mls.h>



cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("kinect2/sd/points", 2, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);

    //    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_segmented", 1);
    //    ind_pub = nh.advertise<pcl_msgs::PointIndices>("point_indices", 1);
    //    coef_pub = nh.advertise<pcl_msgs::ModelCoefficients>("planar_coef", 1);
}

void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

    pcl_conversions::toPCL(input, *cloud_blob);

    std::cerr << "PointCloud before: " << cloud_blob->width * cloud_blob->height << " data points." << std::endl;

    // Convert to the templated PointCloud
    pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_filtered);



    // # # Transformation # #
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    // Define a translation of 2.5 meters on the x axis.
    transform_2.translation() << 0.0, 0.0, -0.6;
    // The same rotation matrix as before; tetha radians arround Z axis
    transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //28deg
    // Print the transformation
    printf ("\nMethod #2: using an Affine3f\n");
    std::cout << transform_2.matrix() << std::endl;
    // Executing the transformation
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
    // You can either apply transform_1 or transform_2; they are the same
    pcl::transformPointCloud (*cloud_filtered, *transformed_cloud, transform_2);

    // PassThrough filter for z-axis
    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
    filter_zrange.setInputCloud(transformed_cloud);
    filter_zrange.setFilterFieldName("z");
    filter_zrange.setFilterLimits(0.0, 0.95);
    filter_zrange.filter(*zrange_filteredCloud);

    // PassThrough filter for y-axis
    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
    filter_yrange.setInputCloud(zrange_filteredCloud);
    filter_yrange.setFilterFieldName("y");
    filter_yrange.setFilterLimits(-2.0, 0.7);
    filter_yrange.filter(*yzrange_filteredCloud);

    std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points." << std::endl;


    // # # SEGMENTATION # #
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);
    //determines how close a point must be to the model in order to be considered an inlier
    seg.setDistanceThreshold (0.01);

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;


    int i = 0, nr_points = (int) yzrange_filteredCloud->points.size ();
    // While 30% of the original cloud is still there
    while (yzrange_filteredCloud->points.size () > 0.3 * nr_points)
    {
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (yzrange_filteredCloud);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0)
        {
            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
        }
        // Extract the inliers
        extract.setInputCloud (yzrange_filteredCloud);
        extract.setIndices (inliers);
        extract.setNegative (false);
        extract.filter (*cloud_p);
        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." <<                                    std::endl;

        //        // Write ervery plane to a seperate pcd file
        //        std::stringstream ss;
        //        ss << "../../../../../plane" << i << ".pcd";
        //        // ss << "table_scene_lms400_plane_" << i << ".pcd";
        //        pcl::PCDWriter writer;
        //        writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

//        // getting the hull of the found plane
//        pcl::PointIndices::Ptr cloud_indices (new pcl::PointIndices ());
//        double z_min = 0.0, z_max =0.7;  // points above the plane with maximum height z_max
//        pcl::PointCloud<pcl::PointXYZ>::Ptr hull_points (new pcl::PointCloud<pcl::PointXYZ> ());
//        pcl::ConvexHull<pcl::PointXYZ> hull;
//        // hull.setDimension (2); // not necessarily needed, but we need to check the dimensionality of the output
//        hull.setInputCloud(cloud_p);
//        //hull.setAlpha(0.1);
//        hull.reconstruct (*hull_points);

//            //to vizualize the computed hull
//            pcl::visualization::CloudViewer viewerplane ("Convex Hull");
//            viewerplane.showCloud(hull_points);
//            while (!viewerplane.wasStopped())
//            {
//                // Do nothing but wait.
//            }

        // Create the filtering object
        extract.setNegative (true);
        extract.filter (*cloud_f);
        yzrange_filteredCloud.swap (cloud_f);
        i++;
        // writer.write("../../../../../filtered_and_downsampled_cloud.pcd", *cloud_filtered);
    }

    // printf(  "\nPoint cloud colors : white  = original point cloud\n"
    //          "                       red    = transformed point cloud\n");


    // getting the hull of the found plane
    pcl::PointIndices::Ptr cloud_indices (new pcl::PointIndices ());
    double z_min = 0.0, z_max =0.7;  // points above the plane with maximum height z_max
    pcl::PointCloud<pcl::PointXYZ>::Ptr hull_points (new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::ConcaveHull<pcl::PointXYZ> hull;
    // hull.setDimension (2); // not necessarily needed, but we need to check the dimensionality of the output
    hull.setInputCloud(cloud_p);
    hull.setAlpha(0.1);
    hull.reconstruct (*hull_points);
    if (hull.getDimension() == 2)
    {
        pcl::ExtractPolygonalPrismData<pcl::PointXYZ> prism;
        prism.setInputCloud(yzrange_filteredCloud);
        prism.setInputPlanarHull(hull_points);
        prism.setHeightLimits(z_min, z_max);
        prism.segment(*cloud_indices);
    }
    // extract the indices above the plane from ititial cloud into a separate one
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ExtractIndices<pcl::PointXYZ> extract_obj;
    extract_obj.setInputCloud (yzrange_filteredCloud);
    extract_obj.setIndices (cloud_indices);
    extract_obj.setNegative (false);
    extract_obj.filter (*cloud_o);

//    //to vizualize the computed hull
//    pcl::visualization::CloudViewer viewerplane ("Convex Hull");
//    viewerplane.showCloud(hull_points);
//    while (!viewerplane.wasStopped())
//    {
//        // Do nothing but wait.
//    }

    //    // The output will also contain the normals.
    //        pcl::PointCloud<pcl::PointNormal>::Ptr smoothedCloud(new pcl::PointCloud<pcl::PointNormal>);
    //    // Smoothing object (we choose what point types we want as input and output).
    //        pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> filter;
    //        filter.setInputCloud(cloud_o);
    //        // Use all neighbors in a radius of 3cm.
    //        filter.setSearchRadius(0.03);
    //        // If true, the surface and normal are approximated using a polynomial estimation
    //        // (if false, only a tangent one).
    //        filter.setPolynomialFit(true);
    //        // We can tell the algorithm to also compute smoothed normals (optional).
    //        filter.setComputeNormals(true);
    //        // kd-tree object for performing searches.
    //        pcl::search::KdTree<pcl::PointXYZ>::Ptr kd_tree ;
    //        filter.setSearchMethod(kd_tree);

    //        filter.process(*smoothedCloud);


    // kd-tree object for searches.
    pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);
    kdtree->setInputCloud(cloud_o);

    // Euclidean clustering object.
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> clustering;
    // Set cluster tolerance to 2cm (small values may cause objects to be divided
    // in several clusters, whereas big values may join objects in a same cluster).
    clustering.setClusterTolerance(0.07);
    // Set the minimum and maximum number of points that a cluster can have.
    clustering.setMinClusterSize(10);
    clustering.setMaxClusterSize(5000);
    clustering.setSearchMethod(kdtree);
    clustering.setInputCloud(cloud_o);
    std::vector<pcl::PointIndices> clusters;

    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*cloud_o,*cloud_o, indices);
    clustering.extract(clusters);

    // For every cluster...
    int currentClusterNum = 1;
    for (std::vector<pcl::PointIndices>::const_iterator i = clusters.begin(); i != clusters.end(); ++i)
    {
        // ...add all its points to a new cloud...
        pcl::PointCloud<pcl::PointXYZ>::Ptr cluster(new pcl::PointCloud<pcl::PointXYZ>);
        for (std::vector<int>::const_iterator point = i->indices.begin(); point != i->indices.end(); point++)
            cluster->points.push_back(cloud_o->points[*point]);
        cluster->width = cluster->points.size();
        cluster->height = 1;
        cluster->is_dense = true;

        // ...and save it to disk.
        if (cluster->points.size() <= 0)
            break;
        std::cout << "Cluster " << currentClusterNum << " has " << cluster->points.size() << " points." << std::endl;
        std::string fileName = "cluster" + boost::to_string(currentClusterNum) + ".pcd";
        pcl::io::savePCDFileASCII(fileName, *cluster);

        currentClusterNum++;
    }

    // # # Visualization # #
    viewer.removeAllPointClouds();
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> range_filteredCloud_color_handler (cloud_o, 230, 20, 20); // Red
    viewer.addPointCloud (cloud_o, range_filteredCloud_color_handler, "range_filteredCloud");
    viewer.addCoordinateSystem(1.0,0);
    viewer.setBackgroundColor(0.05, 0.05, 0.05, 0); // Setting background to a dark grey
    //viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "original_cloud");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "range_filteredCloud");
    //  viewer.setPosition(800, 400); // Setting visualiser window position

    //viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>( yzrange_filteredCloud, normals, 20, 0.03);
    //viewer.addPointCloudNormals<pcl::PointXYZ, pcl::PointNormal>(cloud_o, smoothedCloud,20, 0.03, "normals");

    ros::NodeHandle node;

    tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(0.0, 20.0, 0.0) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "kinect2_link", "new_coord"));
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
//| Help:
//-------
//          p, P   : switch to a point-based representation
//          w, W   : switch to a wireframe-based representation (where available)
//          s, S   : switch to a surface-based representation (where available)

//          j, J   : take a .PNG snapshot of the current window view
//          c, C   : display current camera/window parameters
//          f, F   : fly to point mode

//          e, E   : exit the interactor
//          q, Q   : stop and call VTK's TerminateApp

//           +/-   : increment/decrement overall point size
//     +/- [+ ALT] : zoom in/out

//          g, G   : display scale grid (on/off)
//          u, U   : display lookup table (on/off)

//    r, R [+ ALT] : reset camera [to viewpoint = {0, 0, 0} -> center_{x, y, z}]

//    ALT + s, S   : turn stereo mode on/off
//    ALT + f, F   : switch between maximized window mode and original size

//          l, L           : list all available geometric and color handlers for the current actor map
//    ALT + 0..9 [+ CTRL]  : switch between different geometric handlers (where available)
//          0..9 [+ CTRL]  : switch between different color handlers (where available)

//    SHIFT + left click   : select a point

//          x, X   : toggle rubber band selection mode for left mouse button
