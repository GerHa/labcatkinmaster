
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>

class  cloudHandler
{
public:
    cloudHandler();

    void outputCB(const sensor_msgs::PointCloud2ConstPtr &input);
    void downsampledCB(const sensor_msgs::PointCloud2ConstPtr &input);
    void timerCB(const ros::TimerEvent&);

protected:
    ros::NodeHandle nh;
    ros::Subscriber output_sub, downsampled_sub;
    ros::Publisher downsampled_pub;
    pcl::visualization::PCLVisualizer viewer;
    int output_view, downsampled_view;
    ros::Timer viewer_timer;
};
