
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>

class  cloudHandler
{
public:
    cloudHandler();

    void cloudCB(const sensor_msgs::PointCloud2 &input);

    pcl::PointCloud<pcl::PointXYZ>::Ptr convertPC2toPxyz (const sensor_msgs::PointCloud2 &input);
    //void convertPC2toPxyz (const sensor_msgs::PointCloud2 &input, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered);

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformStartCloud (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered);
    //void transformStartCloud (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered, pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud);
   
    pcl::PointCloud<pcl::PointXYZ>::Ptr passThroughZ (pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud);
    //void passThroughZ (pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud);

    pcl::PointCloud<pcl::PointXYZ>::Ptr passThroughY (pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud);    
    //void passThroughY (pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud);

    void planeSegmentation (pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f);

     pcl::PointCloud<pcl::PointXYZ>::Ptr computePlaneHullAndPrism (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud);
    //void computePlaneHullAndPrism (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o);

    std::vector <pcl::PointIndices> normalEstimationAndClustering(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p);
    //void normalEstimationAndClustering(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,  pcl::PointCloud<pcl::Normal>::Ptr normals, std::vector <pcl::PointIndices> &clusters);

    pcl::PointCloud<pcl::PointXYZ>::Ptr copyAndFilterCluster( pcl::PointCloud<pcl::PointXYZ>::Ptr working_cluster);
    //void copyAndFilterCluster( pcl::PointCloud<pcl::PointXYZ>::Ptr working_cluster, pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster);
    
    Eigen::Vector4f computeClusterCentroid(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o);
    //void computeClusterCentroid(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, Eigen::Vector4f &cluster_centroid);

    // computeBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster, Eigen::Vector4f &cluster_centroid);
    //void computeBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster, Eigen::Vector4f &cluster_centroid, Eigen::Vector3f &bboxTransform, Eigen::Quaternionf &bboxQuaternion, pcl::PointXYZ &minPoint, pcl::PointXYZ &maxPoint);

    void cloudCB2(const sensor_msgs::PointCloud2 &input);
    void timerCB(const ros::TimerEvent&);

protected:
    ros::NodeHandle nh;
    ros::Subscriber pcl_sub;
    ros::Publisher pcl_pub, ind_pub, coef_pub;
    pcl::visualization::PCLVisualizer viewer; //CloudViewer viewer;
    ros::Timer viewer_timer;

};
