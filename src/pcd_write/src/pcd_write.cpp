#include <iostream>
// pcl/io/pcd_io.h is the header that contains the definitions for PCD I/O operations
#include <pcl/io/pcd_io.h>

// pcl/point_types.h contains definitions for several PointT type structures (pcl::PointXYZ in our case)
#include <pcl/point_types.h>


int
  main (int argc, char** argv)
{

  // describes the templated PointCloud structure that we will create
  // the type of each point is set to pcl::PointXYZ,which is:struct PointXYZ{ float x; float y; float z;};
  pcl::PointCloud<pcl::PointXYZ> cloud;

  // Fill in the cloud data with random point values, and set the appropriate parameters (width, height, is_dense)
  cloud.width    = 5;
  cloud.height   = 1;
  cloud.is_dense = false;
  cloud.points.resize (cloud.width * cloud.height);

  for (size_t i = 0; i < cloud.points.size (); ++i)
  {
    cloud.points[i].x = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud.points[i].y = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud.points[i].z = 1024 * rand () / (RAND_MAX + 1.0f);
  }

  // saves the PointCloud data to disk into a file called test_pcd.pcd
  pcl::io::savePCDFileASCII ("../test_pcd_hallo.pcd", cloud);


  // show the data that was generated
  std::cerr << "Saved " << cloud.points.size () << " data points to .pcd." << std::endl;

  for (size_t i = 0; i < cloud.points.size (); ++i)
    std::cerr << "    " << cloud.points[i].x << " " << cloud.points[i].y << " " << cloud.points[i].z << std::endl;

  return (0);
}
