cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(pcd_write)

find_package(PCL 1.2 REQUIRED)
find_package(catkin REQUIRED COMPONENTS
  roscpp
)

catkin_package()

include_directories(${catkin_INCLUDE_DIRS})
include_directories(${PCL_INCLUDE_DIRS})

link_directories(${PCL_LIBRARY_DIRS})

add_definitions(${PCL_DEFINITIONS})

add_executable (pcd_write src/pcd_write.cpp)

target_link_libraries (pcd_write ${PCL_LIBRARIES})
