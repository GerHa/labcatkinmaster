#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include "voxelfilter_frame/cHandler_frame.h"
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <tf/transform_broadcaster.h>
cloudHandler::cloudHandler()
    : viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("kinect2/sd/points", 2, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);

    //    pcl_pub = nh.advertise<sensor_msgs::PointCloud2>("pcl_segmented", 1);
    //    ind_pub = nh.advertise<pcl_msgs::PointIndices>("point_indices", 1);
    //    coef_pub = nh.advertise<pcl_msgs::ModelCoefficients>("planar_coef", 1);
}


void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2), cloud_filtered_blob (new pcl::PCLPointCloud2), cloud_filtered_outl (new pcl::PCLPointCloud2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

    // Fill in the cloud data
    //    pcl::PCDReader reader;
    //    reader.read ("/home/gerry/catkin_ws/table_scene_lms400.pcd", *cloud_blob);
    pcl_conversions::toPCL(input, *cloud_blob);


    std::cerr << "PointCloud before filtering: " << cloud_blob->width * cloud_blob->height << " data points." << std::endl;

    //Filtering
    pcl::StatisticalOutlierRemoval<pcl::PCLPointCloud2> statFilter;
    statFilter.setInputCloud(cloud_blob);
    statFilter.setMeanK(10);
    statFilter.setStddevMulThresh(0.2);
    statFilter.filter(*cloud_filtered_outl);

    //Downsampling
    // pcl::PointCloud<pcl::PointXYZ> cloud_downsampled;
    pcl::VoxelGrid<pcl::PCLPointCloud2> voxelSampler;
    voxelSampler.setInputCloud(cloud_filtered_outl);
    voxelSampler.setLeafSize(0.01f, 0.01f, 0.01f);
    voxelSampler.filter(*cloud_filtered_blob);

    // Convert to the templated PointCloud
    pcl::fromPCLPointCloud2 (*cloud_filtered_blob, *cloud_filtered);

    std::cerr << "PointCloud after filtering: " << cloud_filtered->width * cloud_filtered->height << " data points." << std::endl;

    //    // Write the downsampled version to disk
    //    pcl::PCDWriter writer;
    //    writer.write<pcl::PointXYZ> ("../../../../../table_scene_lms400_downsampled.pcd", *cloud_filtered, false);


    //    //SEGMENTATION
    //    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    //    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    //    // Create the segmentation object
    //    pcl::SACSegmentation<pcl::PointXYZ> seg;
    //    // Optional
    //    seg.setOptimizeCoefficients (true);
    //    // Mandatory
    //    seg.setModelType (pcl::SACMODEL_PLANE);
    //    seg.setMethodType (pcl::SAC_RANSAC);
    //    seg.setMaxIterations (1000);
    //    seg.setDistanceThreshold (0.01);

    //    // Create the filtering object
    //    pcl::ExtractIndices<pcl::PointXYZ> extract;

    //    int i = 0, nr_points = (int) cloud_filtered->points.size ();
    //    // While 30% of the original cloud is still there
    //    while (cloud_filtered->points.size () > 0.2 * nr_points)
    //    {
    //        // Segment the largest planar component from the remaining cloud
    //        seg.setInputCloud (cloud_filtered);
    //        seg.segment (*inliers, *coefficients);
    //        if (inliers->indices.size () == 0)
    //        {
    //            std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
    //            break;
    //        }

    //        // Extract the inliers
    //        extract.setInputCloud (cloud_filtered);
    //        extract.setIndices (inliers);
    //        extract.setNegative (false);
    //        extract.filter (*cloud_p);
    //        std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

    //        std::stringstream ss;
    //        ss << "../../../../../plane" << i << ".pcd";
    //        //ss << "table_scene_lms400_plane_" << i << ".pcd";
    //        writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

    //        // Create the filtering object
    //        extract.setNegative (true);
    //        extract.filter (*cloud_f);
    //        cloud_filtered.swap (cloud_f);
    //        i++;
    //        //writer.write("../../../../../filtered_and_downsampled_cloud.pcd", *cloud_filtered);
    //    }


    /*  METHOD #2: Using a Affine3f
     *
     *
     *
      This method is easier and less error prone
    */
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();

    // Define a translation of 2.5 meters on the x axis.
    transform_2.translation() << 0.0, 0.0, -0.75;

    // The same rotation matrix as before; tetha radians arround Z axis
      transform_2.rotate (Eigen::AngleAxisf (-0.34906585039887, Eigen::Vector3f::UnitX()));

    // Print the transformation
    printf ("\nMethod #2: using an Affine3f\n");
    std::cout << transform_2.matrix() << std::endl;

    // Executing the transformation
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
    // You can either apply transform_1 or transform_2; they are the same
    pcl::transformPointCloud (*cloud_filtered, *transformed_cloud, transform_2);


    // Visualization
    printf(  "\nPoint cloud colors : white  = original point cloud\n"
             "                       red    = transformed point cloud\n");
    //pcl::visualization::PCLVisualizer viewerpcl ("PCL Viewer");
    viewer.removeAllPointClouds();
    // Define R,G,B colors for the point cloud
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> source_cloud_color_handler (cloud_filtered, 255, 255, 255);
    // We add the point cloud to the viewer and pass the color handler
    viewer.addPointCloud (cloud_filtered, source_cloud_color_handler, "original_cloud");

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> transformed_cloud_color_handler (transformed_cloud, 230, 20, 20); // Red
    viewer.addPointCloud (transformed_cloud, transformed_cloud_color_handler, "transformed_cloud");

    viewer.addCoordinateSystem(1.0,0);
    viewer.setBackgroundColor(0.05, 0.05, 0.05, 0); // Setting background to a dark grey
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "original_cloud");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "transformed_cloud");
    //  viewer.setPosition(800, 400); // Setting visualiser window position

    //    while (!viewer.wasStopped ()) { // Display the visualiser until 'q' key is pressed
    //      viewer.spinOnce ();
    //    }

    //viewer.showCloud(transformed_cloud);

    ros::NodeHandle node;

    tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(0.0, 20.0, 0.0) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "kinect2_link", "new_coord"));
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
//| Help:
//-------
//          p, P   : switch to a point-based representation
//          w, W   : switch to a wireframe-based representation (where available)
//          s, S   : switch to a surface-based representation (where available)

//          j, J   : take a .PNG snapshot of the current window view
//          c, C   : display current camera/window parameters
//          f, F   : fly to point mode

//          e, E   : exit the interactor
//          q, Q   : stop and call VTK's TerminateApp

//           +/-   : increment/decrement overall point size
//     +/- [+ ALT] : zoom in/out

//          g, G   : display scale grid (on/off)
//          u, U   : display lookup table (on/off)

//    r, R [+ ALT] : reset camera [to viewpoint = {0, 0, 0} -> center_{x, y, z}]

//    ALT + s, S   : turn stereo mode on/off
//    ALT + f, F   : switch between maximized window mode and original size

//          l, L           : list all available geometric and color handlers for the current actor map
//    ALT + 0..9 [+ CTRL]  : switch between different geometric handlers (where available)
//          0..9 [+ CTRL]  : switch between different color handlers (where available)

//    SHIFT + left click   : select a point

//          x, X   : toggle rubber band selection mode for left mouse button
