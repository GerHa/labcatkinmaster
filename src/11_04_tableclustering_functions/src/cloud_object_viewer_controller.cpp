#include "cloud_object_viewer_controller.h"
#include "object_detector.h"
#include "ros_simplyfier.h"
#include "transformations.h"

namespace ObjectCloudViewer {

Controller::Controller(ObjectDetector::Detector *ctrl)
    : viewer("Cloud Viewer")
{
    ObjectDetectorDetector = ctrl;
    pcl_sub = nh.subscribe("kinect2/sd/points",5, &Controller::cloudCB, this);
    //create the world coordinate system
    viewer.addCoordinateSystem(.1,0);
}

void
Controller::cloudCB
(const sensor_msgs::PointCloud2 &input)
{
    // cleanup the old cloud and bounding box
    viewer.removeAllPointClouds();
    viewer.removeAllShapes();
    //viewer.removeCoordinateSystem();

    // recreate the world coordinate system
    // viewer.addCoordinateSystem(.1,0);

    // add the rest to the viewer window
    if( ObjectDetectorDetector != NULL ) {
        ObjectDetectorDetector->get_information(input, &viewer);
    }
    std::cout << "finish()" << std::endl;

    render();

}

void
Controller::render()
{
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}

void
Controller::timerCB
(const ros::TimerEvent&)
{
    std::cout << "timerCB called" << std::cout;
    render();
}
} // namespace
