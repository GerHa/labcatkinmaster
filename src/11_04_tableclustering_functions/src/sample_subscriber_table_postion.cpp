#include <ros/ros.h>
#include "table_detector.h"
#include "table_entity.h"
#include "cloud_viewer_controller.h"
#include "TablePosition.h"
#include <stdio.h>

/*
 * This is a sample for a subscriber that can subscribe to the "table_position" topic to get the information aboutthe center   of the table
 */

void objectPositionCB(const geha::TablePosition::ConstPtr& msg)
{
    std::cout << "tablePositionCB" << std::endl;
    std::cout << "x = " << msg->center_x_coordinate << std::endl;
    std::cout << "y = " << msg->center_y_coordinate << std::endl;
    std::cout << "z = " << msg->center_z_coordinate << std::endl;

}

main (int argc, char **argv)
{
    ros::init (argc, argv, "table_position_subscriber");
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("table_position", 1, objectPositionCB);

    ros::spin();

    return 0;
}
