
#include "table_detector.h"
#include "table_geometry_shenanigans.h"
#include "transformations.h"
#include "ros_simplyfier.h"
#include <sys/select.h>

namespace TableDetector {

Detector::Detector()
{
}

bool
Detector::any_key_pressed()
{
    bool return_value = false;
    // this is a keypress logic
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    int ret = select(1, &fds, NULL, NULL, &tv);
    if(ret != 0)
        return_value = true;
    return ret;
}

void
Detector::go()
{
    while (ros::ok())
    {
        if (any_key_pressed())
            break;

        ros::spinOnce();
    }
}

TableEntity
Detector::get_table_from_point_cloud
( const sensor_msgs::PointCloud2 &input_cloud_data ) {
    std::cout << "cloudCB() called" << std::endl;

    TableEntity table;
    pointXYZ_PTR input_cloud;
    pointXYZ_PTR transformed_cloud;
    pointXYZ_PTR zrange_filteredCloud;
    pointXYZ_PTR yzrange_filteredCloud;
    pointXYZ_PTR table_points;

    input_cloud = transformations::convertPC2toPxyz(input_cloud_data);
    transformed_cloud = transformStartCloud(input_cloud);
    zrange_filteredCloud = passThroughZ(transformed_cloud);

    std::cout << "passThroughZ() done" << std::endl;

    yzrange_filteredCloud = passThroughY(zrange_filteredCloud);

    std::cout << "passThroughY() done" << std::endl;

    table_points = planeSegmentation(yzrange_filteredCloud);

    std::cout << "planeSegmentation() done" << std::endl;

    Eigen::Vector4f cluster_centroid;
    cluster_centroid = computeClusterCentroid(table_points);

    std::cout << "computeClusterCentroid() done" << std::endl;

    table = computeBoundingBox(table_points, cluster_centroid);

    std::cout << "computeBoundingBox() done" << std::endl;

    table.cloud = table_points;

    return table;
}

void
Detector::compute_table_and_fill_view
(const sensor_msgs::PointCloud2 &input_cloud_data, pcl::visualization::PCLVisualizer *viewer)
{
    TableEntity table = get_table_from_point_cloud( input_cloud_data );

    viewer->addPointCloud(table.cloud);

    viewer->addCube(table.bboxTransform, table.bboxQuaternion,
                   table.maxPoint.x - table.minPoint.x,
                   table.maxPoint.y - table.minPoint.y,
                   table.maxPoint.z - table.minPoint.z);

    viewer->addCoordinateSystem(.1,
                            table.bboxTransform[0],
                            table.bboxTransform[1],
                            table.bboxTransform[2]);

    _table = table;
}

TableEntity
Detector::get_table()
{
    return _table;
}

Detector::~Detector()
{

}

} // namespace end
