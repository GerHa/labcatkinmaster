
#include <ros/ros.h>
#include "table_detector.h"
#include "table_entity.h"
#include "cloud_viewer_controller.h"
#include "TablePosition.h"
#include <stdio.h>

ros::Publisher publisher;
ros::Subscriber subscriber;
ros::Timer viewer_timer;

geha::TablePosition message;

void table_position
(const ros::TimerEvent&)
{
    std::cout << "table_position publishing..." << std::cout;
    publisher.publish(message);
    std::cout << "table_position done." << std::endl;

}

void incommingMeasurement
(const geha::TablePosition::ConstPtr& msg)
{
    std::cout << "Table Position Provider recieved something..." << std::endl;
    std::cout << "x = " << msg->center_x_coordinate << std::endl;
    std::cout << "y = " << msg->center_y_coordinate << std::endl;
    std::cout << "z = " << msg->center_z_coordinate << std::endl;

    std::cout << "Table Position Provider saving message..." << std::endl;

    message.center_x_coordinate = msg->center_x_coordinate;
    message.center_y_coordinate = msg->center_y_coordinate;
    message.center_z_coordinate = msg->center_z_coordinate;

    std::cout << "Table Position Provider done." << std::endl;
}

main (int argc, char **argv)
{
    ros::init (argc, argv, "table_position_provider");

    ros::NodeHandle nh;

    publisher =
            nh.advertise<geha::TablePosition> ("table_position", 1);

    subscriber =
            nh.subscribe("table_position_measurement", 1, incommingMeasurement);

    viewer_timer = nh.createTimer(ros::Duration(0.5), &table_position);

    ros::spin();

    return 0;
}
