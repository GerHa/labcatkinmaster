#include <ros/ros.h>
#include "table_detector.h"
#include "table_entity.h"
#include "cloud_viewer_controller.h"
#include "ObjectPosition.h"
#include "TablePosition.h"

#include <stdio.h>

std::vector<geha::ObjectPosition::ConstPtr> object_messages;
geha::TablePosition::ConstPtr table_message;


void tablePositionCB(const geha::TablePosition::ConstPtr& msg)
{
    table_message = msg;
}

void objectPositionCB(const geha::ObjectPosition::ConstPtr& msg)
{
    int object_number = msg->object_number;
    std::cout << "Object number is: 0x" << std::hex << object_number << std::endl;

    if( object_number < 0xFF )
    {
        object_messages.push_back(msg);
        std::cout << "message_stored! Size: " << object_messages.size() << " messages" << std::endl;

    } else {
        std::cout << "New scene started" << std::endl;
        std::cout << "  Processing " << object_messages.size() << " messages" << std::endl;

        if (table_message == NULL)
        {
            object_messages.clear();
            return;
        }

        double x_table = table_message->center_x_coordinate;
        double y_table = table_message->center_y_coordinate;
        double z_table = table_message->center_z_coordinate;

        for( int i = 0; i < object_messages.size(); i++ )
        {
            geha::ObjectPosition::ConstPtr object_message = object_messages[i];
            double x_object = object_messages[i]->center_x_coordinate;
            double y_object = object_messages[i]->center_y_coordinate;
            double z_object = object_messages[i]->center_z_coordinate;

            double distance = (x_object - x_table) * (x_object - x_table);
            distance += (y_object - y_table) * (y_object - y_table);
            distance += (z_object - z_table) * (z_object - z_table);
            distance = sqrt(distance);

            std::cout << "  distance[" << std::dec << i << "] =" << distance << "parsec" << std::endl;

        }
        object_messages.clear();

    }

}

main (int argc, char **argv)
{
    ros::init (argc, argv, "object_position_subscriber");

    ros::NodeHandle nh;
    ros::Subscriber object_subscriber = nh.subscribe("detected_objects", 1, objectPositionCB);
    ros::Subscriber table_subscriber = nh.subscribe("table_position", 1, tablePositionCB);

    ros::spin();

    return 0;
}
