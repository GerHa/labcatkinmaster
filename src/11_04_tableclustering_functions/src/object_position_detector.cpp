#include <ros/ros.h>
#include "object_detector.h"
#include "objects.h"
#include "cloud_object_viewer_controller.h"
#include "ObjectPosition.h"
#include <stdio.h>



main (int argc, char **argv)
{

    ros::init (argc, argv, "object_position_detector");

    ObjectDetector::Detector Detector;

    ObjectCloudViewer::Controller cloudViewController(&Detector);

    Detector.go();

    return 0;
}

