#include "cloud_viewer_controller.h"
#include "table_detector.h"
#include "ros_simplyfier.h"
#include "transformations.h"

namespace CloudViewer {

Controller::Controller(TableDetector::Detector *ctrl)
    : viewer("Cloud Viewer")
{
    TableDetectorDetector = ctrl;
    pcl_sub = nh.subscribe("kinect2/sd/points",5, &Controller::cloudCB, this);
    viewer.addCoordinateSystem(.1,0);
}

void
Controller::cloudCB
(const sensor_msgs::PointCloud2 &input)
{
    // cleanup the old cloud and bounding box
    viewer.removeAllPointClouds();
    viewer.removeAllShapes();
    viewer.removeCoordinateSystem();

    // recreate the world coordinate system
    viewer.addCoordinateSystem(.1,0);

    // add the rest to the viewer window
    if( TableDetectorDetector != NULL ) {
        TableDetectorDetector->compute_table_and_fill_view(input, &viewer);
    }
    std::cout << "finish()" << std::endl;

    render();

}

void
Controller::render()
{
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}

void
Controller::timerCB
(const ros::TimerEvent&)
{
    std::cout << "timerCB called" << std::cout;
    render();
}
} // namespace
