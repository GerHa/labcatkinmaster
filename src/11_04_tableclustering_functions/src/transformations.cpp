
#include <ros/ros.h>
//#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
//#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include "transformations.h"

pcl::PointCloud<pcl::PointXYZ>::Ptr
transformations::convertPC2toPxyz
(const sensor_msgs::PointCloud2 &input)
{
        // Convert to the templated PointCloud
        pcl::PCLPointCloud2::Ptr pcl_PC2 (new pcl::PCLPointCloud2);
        pcl_conversions::toPCL(input, *pcl_PC2);
        pcl::PointCloud<pcl::PointXYZ>::Ptr pointXYZPtr (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::fromPCLPointCloud2 (*pcl_PC2, *pointXYZPtr);

        return pointXYZPtr;

}
