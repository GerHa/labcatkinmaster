#include <ros/ros.h>
#include "table_detector.h"
#include "table_entity.h"
#include "cloud_viewer_controller.h"
#include "TablePosition.h"
#include <stdio.h>

main (int argc, char **argv)
{
    // this is the storage for the detected table after the Detector terminates
    TableDetector::TableEntity detectedTable;
    // this resembles the message that will be published
    geha::TablePosition msg;

    ros::init (argc, argv, "table_position_detector");
    ros::NodeHandle nh;
    ros::Publisher publisher =
            nh.advertise<geha::TablePosition> ("table_position_measurement", 1);

    TableDetector::Detector Detector;

    CloudViewer::Controller cloudViewController(&Detector);

    Detector.go();

    detectedTable = Detector.get_table();

    msg.center_x_coordinate = detectedTable.bboxTransform[0];
    msg.center_y_coordinate = detectedTable.bboxTransform[1];
    msg.center_z_coordinate = detectedTable.bboxTransform[2];

    publisher.publish(msg);

    return 0;
}
