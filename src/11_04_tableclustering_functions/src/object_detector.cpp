
#include "object_detector.h"
#include "object_geometry_shenanigans.h"
#include "transformations.h"
#include "ros_simplyfier.h"
#include <sys/select.h>
#include "ObjectPosition.h"
#include <stdio.h>


namespace ObjectDetector {

Detector::Detector()
{
}

bool
Detector::any_key_pressed()
{
    bool return_value = false;
    // this is a keypress logic
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    int ret = select(1, &fds, NULL, NULL, &tv);
    if(ret != 0)
        return_value = true;
    return ret;
}

void
Detector::go()
{
    while (ros::ok())
    {
        if (any_key_pressed())
            break;

        ros::spinOnce();
    }
}

Objects
Detector::get_objects_from_point_cloud
( const sensor_msgs::PointCloud2 &input_cloud_data ) {
    std::cout << "cloudCB() called" << std::endl;

    Objects return_objects;

    pointXYZ_PTR input_cloud;
    pointXYZ_PTR transformed_cloud;
    pointXYZ_PTR zrange_filteredCloud;
    pointXYZ_PTR yzrange_filteredCloud;
    pointXYZ_PTR table_points;
    pointXYZ_PTR not_table_points;

    input_cloud = transformations::convertPC2toPxyz(input_cloud_data);
    transformed_cloud = transformStartCloud(input_cloud);
    zrange_filteredCloud = passThroughZ(transformed_cloud);
    std::cout << "passThroughZ() done" << std::endl;

    yzrange_filteredCloud = passThroughY(zrange_filteredCloud);
    std::cout << "passThroughY() done" << std::endl;

    table_points = planeSegmentation(yzrange_filteredCloud, false );
    not_table_points = planeSegmentation(yzrange_filteredCloud, true );
    std::cout << "planeSegmentation() done" << std::endl;

    yzrange_filteredCloud.swap(not_table_points);
    std::cout << "swap(not_table_points) done" << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr prism_content =
            computePlaneHullAndPrism(table_points, yzrange_filteredCloud);
    std::cout << "computePlaneHullAndPrism(table_points) done" << std::endl;

    std::vector <pcl::PointIndices> clusters =
            normalEstimationAndClustering(prism_content);
    std::cout << "normalEstimationAndClustering(prism_content) done" << std::endl;

    // For every cluster...
    int currentClusterNum = 1;

    for (std::vector<pcl::PointIndices>::const_iterator cluster = clusters.begin(); cluster != clusters.end(); ++cluster)
    {
        /*
         * Create a copy of the clustered points; we use the copy constructor to extract
         * the points corresponding to a single cluster
         */
        pcl::PointCloud<pcl::PointXYZ>::Ptr working_cluster(new
                                pcl::PointCloud<pcl::PointXYZ>(*prism_content, cluster->indices));
        pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster = copyAndFilterCluster(working_cluster);

        Eigen::Vector4f cluster_centroid =
                computeClusterCentroid(filtered_cluster);

        Object object =
                computeBoundingBox(filtered_cluster, cluster_centroid);

        object.cloud = filtered_cluster;
        object.table = table_points;
        return_objects.push_back(object);

        currentClusterNum++;
    }

    return return_objects;
}

void
Detector::get_information
(const sensor_msgs::PointCloud2 &input_cloud_data, pcl::visualization::PCLVisualizer *viewer)
{
    ros::NodeHandle noha;

    ros::Publisher objectPublisher =
            noha.advertise<geha::ObjectPosition> ("detected_objects", 1);

    Objects objects = get_objects_from_point_cloud( input_cloud_data );

    // viewer->removeAllShapes();
    viewer->addPointCloud(objects[0].table);

    std::cout << "Objects size is: " << objects.size() << std::endl;

    geha::ObjectPosition new_scene_msg;
    new_scene_msg.object_number = (uint8_t) 0xFF;
    objectPublisher.publish(new_scene_msg);
    std::cout << "new_scene_msg:\n" << new_scene_msg << std::endl;

    for( int i = 0; i < objects.size(); i++ ) {

        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> ColorHandlerXYZ (objects[i].cloud, 51, 153, 0);
        std::string Objectnumber = "Objectnumber" + boost::to_string(i);
        viewer->addPointCloud(objects[i].cloud , ColorHandlerXYZ, Objectnumber);


        std::string boxCenterName = "bboxCenter" + boost::to_string(i);
        viewer->addCube(objects[i].bboxTransform, objects[i].bboxQuaternion,
                        objects[i].maxPoint.x - objects[i].minPoint.x,
                        objects[i].maxPoint.y - objects[i].minPoint.y,
                        objects[i].maxPoint.z - objects[i].minPoint.z,
                        boxCenterName);

//        viewer->addCoordinateSystem(.05,
//                                    objects[i].bboxTransform[0],
//                                    objects[i].bboxTransform[1],
//                                    objects[i].bboxTransform[2],
//                                    0);


        geha::ObjectPosition msg;
        msg.object_number = (uint8_t) i;
        msg.center_x_coordinate = objects[i].bboxTransform[0];
        msg.center_y_coordinate = objects[i].bboxTransform[1];
        msg.center_z_coordinate = objects[i].bboxTransform[2];

        msg.quaternion_x = objects[i].bboxQuaternion.x();
        msg.quaternion_y = objects[i].bboxQuaternion.y();
        msg.quaternion_z = objects[i].bboxQuaternion.z();
        msg.quaternion_w = objects[i].bboxQuaternion.w();

        std::cout << "Quaternion W:" << msg.quaternion_w << std::endl;

        msg.maxPointx = objects[i].maxPoint.x;
        msg.minPointx = objects[i].minPoint.x;
        msg.maxPointy = objects[i].maxPoint.y;
        msg.minPointy = objects[i].minPoint.y;
        msg.maxPointz = objects[i].maxPoint.z;
        msg.minPointz = objects[i].minPoint.z;


        objectPublisher.publish(msg);
        std::cout << "Message:\n" << msg << std::endl;

    }
}

Detector::~Detector()
{

}

} // namespace end
