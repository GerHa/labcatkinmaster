cmake_minimum_required(VERSION 2.8.3)

project(geha)

find_package(catkin REQUIRED COMPONENTS
  pcl_conversions
  pcl_msgs
  pcl_ros
  roscpp
  sensor_msgs
  std_msgs
  message_generation
)

find_package(PCL REQUIRED)

add_message_files(
  FILES
  TablePosition.msg
  ObjectPosition.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(CATKIN_DEPENDS message_runtime)

include_directories(${catkin_INCLUDE_DIRS})
include_directories(include ${PCL_INCLUDE_DIRS})

link_directories(${PCL_LIBRARY_DIRS})

# specify the executables
add_executable(table_position_detector
                src/table_position_detector.cpp)

add_executable(object_position_detector
                src/object_position_detector.cpp)

add_executable(sample_table_positon_subscriber
                src/sample_subscriber_table_postion.cpp)

add_executable(sample_object_positon_subscriber
                src/sample_subscriber_object_postion.cpp)

add_executable(table_position_provider
                src/table_position_provider.cpp)

# define the libraries
add_library(views
                src/cloud_viewer_controller.cpp
                src/cloud_object_viewer_controller.cpp)

add_library(detectors
                src/table_detector.cpp
                src/object_detector.cpp)

add_library(transformations
                src/transformations.cpp)

# link the stuff into the libs
target_link_libraries(views ${catkin_LIBRARIES} ${PCL_LIBRARIES})
target_link_libraries(detectors views ${catkin_LIBRARIES} ${PCL_LIBRARIES})

# link the executable
target_link_libraries(table_position_detector views detectors transformations ${catkin_LIBRARIES} ${PCL_LIBRARIES})

target_link_libraries(object_position_detector views detectors transformations ${catkin_LIBRARIES} ${PCL_LIBRARIES})

target_link_libraries(sample_table_positon_subscriber ${catkin_LIBRARIES} ${PCL_LIBRARIES})

target_link_libraries(sample_object_positon_subscriber ${catkin_LIBRARIES} ${PCL_LIBRARIES})

target_link_libraries(table_position_provider ${catkin_LIBRARIES} ${PCL_LIBRARIES})
