#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/common/transforms.h>
#include <pcl/common/pca.h>

#ifndef _TABLE_EXTRACTION
#define _TABLE_EXTRACTION

namespace TableDetector {

pcl::PointCloud<pcl::PointXYZ>::Ptr
transformStartCloud
(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud)
{
    // # # Transformation # #
    Eigen::Affine3f transform = Eigen::Affine3f::Identity();
    // Define a translation vector of x y z.
    transform.translation() << 0.0, 0.0, -0.65;
    transform.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX()));
    // Print the transformation
    //std::cout << transform.matrix() << std::endl;
    // Executing the transformation
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_temp (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud (*input_cloud, *transformed_cloud_temp, transform);

    return transformed_cloud_temp;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
passThroughZ
(pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud)
{

    // PassThrough filter for z-axis
    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_zrange.setInputCloud(transformed_cloud);
    filter_zrange.setFilterFieldName("z");
    filter_zrange.setFilterLimits(0.0, 0.95);
    filter_zrange.filter(*zrange_filteredCloud);
    std::cerr << "zRangeFiltered: " << zrange_filteredCloud->width * zrange_filteredCloud->height << " data points." << std::endl;

    return zrange_filteredCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
passThroughY
(pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud)
{

    // PassThrough filter for y-axis
    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_yrange.setInputCloud(zrange_filteredCloud);
    filter_yrange.setFilterFieldName("y");
    filter_yrange.setFilterLimits(-2.0, 0.7);
    filter_yrange.filter(*yzrange_filteredCloud);

//    std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points."
//              << std::endl;
    return yzrange_filteredCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
planeSegmentation
(pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud)
{

    // # # SEGMENTATION # #
    // create segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> segmentation;
    // optional
    segmentation.setOptimizeCoefficients (true);
    // mandatory
    segmentation.setModelType (pcl::SACMODEL_PLANE);
    segmentation.setMethodType (pcl::SAC_RANSAC);
    segmentation.setMaxIterations (1000);
    //determines how close a point must be to the model in order to be considered an inlier
    segmentation.setDistanceThreshold (0.01);

    // create filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
     pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    // segment the largest planar component from the remaining cloud
    segmentation.setInputCloud (yzrange_filteredCloud);
    segmentation.segment (*inliers, *coefficients);

    // extract the inliers
    pcl::PointCloud<pcl::PointXYZ>::Ptr table_points (new pcl::PointCloud<pcl::PointXYZ>);
    extract.setInputCloud (yzrange_filteredCloud);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*table_points);
    return table_points;
}


Eigen::Vector4f
computeClusterCentroid
(pcl::PointCloud<pcl::PointXYZ>::Ptr table_points)
{
    Eigen::Vector4f cluster_centroid;

    pcl::compute3DCentroid(*table_points, cluster_centroid);
    std::cout << "Cluster Centroid " << cluster_centroid[0] << " " << cluster_centroid[1] << " " << cluster_centroid[2]
              << " "  << cluster_centroid[3] << std::endl;

    return cluster_centroid;
}

TableDetector::TableEntity
computeBoundingBox
(pcl::PointCloud<pcl::PointXYZ>::Ptr table_points,
 Eigen::Vector4f &cluster_centroid)
{
    TableDetector::TableEntity table;

    // compute principal directions
    Eigen::Matrix3f covariance;
    computeCovarianceMatrixNormalized(*table_points, cluster_centroid, covariance);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
    Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();

    // This line is necessary for proper orientation in some cases. The numbers come out the same without it,
    // but the signs are different and the box doesn't get correctly oriented in some cases.
    //std::cout << "Eigenvectors eigenVectorPCA before :\n" << eigenVectorsPCA << std::endl;
    eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));

    // Transform the original cloud to the origin where the principal components correspond to the axes.
    Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
    projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
    projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * cluster_centroid.head<3>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*table_points, *cloudPointsProjected, projectionTransform);

    // Get the minimum and maximum points of the transformed cloud.
    // pcl::PointXYZ minPoint, maxPoint;
    pcl::getMinMax3D(*cloudPointsProjected, table.minPoint, table.maxPoint);
    const Eigen::Vector3f meanDiagonal = 0.5f*(table.maxPoint.getVector3fMap() + table.minPoint.getVector3fMap());

    std::cout << "minPoint: " << table.minPoint << "; maxPoint:  " << table.maxPoint << " " << std::endl;
    std::cout << "meanDiagonal:" << meanDiagonal << std::endl;

    // Final transform
    const Eigen::Quaternionf bboxQuaternion_temp(eigenVectorsPCA);
    table.bboxQuaternion = bboxQuaternion_temp;
    // Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
    //                      3x1               3x3              3x1              4x1
    // bboxTransform ist der Mittelpunkt der Box um das jeweilige Cluster
    const Eigen::Vector3f bboxTransform_temp = eigenVectorsPCA * meanDiagonal + cluster_centroid.head<3>();
    table.bboxTransform = bboxTransform_temp;

    return table;
}
} //end namespace
#endif
