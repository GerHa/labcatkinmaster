#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include "table_entity.h"

#ifndef _TABLE_DETECTOR
#define _TABLE_DETECTOR

namespace TableDetector {
class Detector {
    TableEntity _table;

public:
    Detector();
    ~Detector();

    TableEntity
    get_table_from_point_cloud
    ( const sensor_msgs::PointCloud2 & );

    void
    compute_table_and_fill_view
    (const sensor_msgs::PointCloud2 &,
     pcl::visualization::PCLVisualizer *);

    void go();

    bool
    any_key_pressed();

    TableEntity
    get_table();

};
} // namespace end

#endif
