#include "objects.h"
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>
#include <object_detector.h>

namespace ObjectCloudViewer {
class  Controller
{
public:
    Controller(ObjectDetector::Detector *ctrl);

    void cloudCB(const sensor_msgs::PointCloud2 &input);
    void timerCB(const ros::TimerEvent&);

protected:
    pcl::visualization::PCLVisualizer viewer; //CloudViewer viewer;

private:
    void render();
    ObjectDetector::Detector *ObjectDetectorDetector;
    ros::NodeHandle nh;
    ros::Subscriber pcl_sub;

};

} // namespace cloud_viewer
