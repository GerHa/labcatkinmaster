
#include "table_entity.h"
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>
#include <table_detector.h>

namespace CloudViewer {
class  Controller
{
public:
    Controller(TableDetector::Detector *ctrl);

    void cloudCB(const sensor_msgs::PointCloud2 &input);
    void timerCB(const ros::TimerEvent&);

protected:
    pcl::visualization::PCLVisualizer viewer;

private:
    void render();
    TableDetector::Detector *TableDetectorDetector;
    ros::NodeHandle nh;
    ros::Subscriber pcl_sub;

};

} // namespace cloud_viewer
