#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/common/transforms.h>
#include <pcl/common/pca.h>

#ifndef _OBJECT_EXTRACTION
#define _OBJECT_EXTRACTION

namespace ObjectDetector {

pcl::PointCloud<pcl::PointXYZ>::Ptr
transformStartCloud
(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud)
{
    // # # Transformation # #
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    // Define a translation vector of x y z.
    transform_2.translation() << 0.0, 0.0, -0.65;
    transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
    // Print the transformation
    //std::cout << transform_2.matrix() << std::endl;
    // Executing the transformation
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_temp (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud (*input_cloud, *transformed_cloud_temp, transform_2);

    return transformed_cloud_temp;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
passThroughZ
(pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud)
{

    // PassThrough filter for z-axis
    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_zrange.setInputCloud(transformed_cloud);
    filter_zrange.setFilterFieldName("z");
    filter_zrange.setFilterLimits(0.0, 0.95);
    filter_zrange.filter(*zrange_filteredCloud);
    std::cerr << "zRangeFiltered: " << zrange_filteredCloud->width * zrange_filteredCloud->height << " data points." << std::endl;

    return zrange_filteredCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
passThroughY
(pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud)
{

    //   PassThrough filter for y-axis
    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_yrange.setInputCloud(zrange_filteredCloud);
    filter_yrange.setFilterFieldName("y");
    filter_yrange.setFilterLimits(-2.0, 0.7);
    filter_yrange.filter(*yzrange_filteredCloud);

//    std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points."
//              << std::endl;
    return yzrange_filteredCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
planeSegmentation
(pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud, bool set_negative)
{

    // # # SEGMENTATION # #
    // create segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> segmentation;
    // create filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud (new pcl::PointCloud<pcl::PointXYZ>);

    // Optional
    segmentation.setOptimizeCoefficients (true);
    // Mandatory
    segmentation.setModelType (pcl::SACMODEL_PLANE);
    segmentation.setMethodType (pcl::SAC_RANSAC);
    segmentation.setMaxIterations (1000);
    //determines how close a point must be to the model in order to be considered an inlier
    segmentation.setDistanceThreshold (0.01);

    // Segment the largest planar component from the remaining cloud
    segmentation.setInputCloud (yzrange_filteredCloud);
    segmentation.segment (*inliers, *coefficients);

    //Extract the inliers
    extract.setInputCloud (yzrange_filteredCloud);
    extract.setIndices (inliers);
    extract.setNegative (set_negative);
    extract.filter (*output_cloud);
    return output_cloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
computePlaneHullAndPrism
(pcl::PointCloud<pcl::PointXYZ>::Ptr table_points, pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud)
{
     // getting the hull of the found plane
     pcl::PointIndices::Ptr cloud_indices (new pcl::PointIndices ());
     double z_min = 0., z_max =0.5;  // points above the plane with maximum height z_max
     pcl::PointCloud<pcl::PointXYZ>::Ptr hull_points (new pcl::PointCloud<pcl::PointXYZ> ());
     pcl::ConvexHull<pcl::PointXYZ> hull;
     // hull.setDimension (2); // not necessarily needed, but we need to check the dimensionality of the output
     hull.setInputCloud(table_points);
     //hull.setAlpha(0.1);
     hull.reconstruct (*hull_points);
     if (hull.getDimension() == 2)
     {
         pcl::ExtractPolygonalPrismData<pcl::PointXYZ> prism;
         prism.setInputCloud(yzrange_filteredCloud);
         prism.setInputPlanarHull(hull_points);
         prism.setHeightLimits(z_min, z_max);
         prism.segment(*cloud_indices);
     }
     // extract the indices above the plane from ititial cloud into a separate one
     pcl::ExtractIndices<pcl::PointXYZ> extract_obj;
     pcl::PointCloud<pcl::PointXYZ>::Ptr prism_content (new pcl::PointCloud<pcl::PointXYZ>);
     extract_obj.setInputCloud (yzrange_filteredCloud);
     extract_obj.setIndices (cloud_indices);
     extract_obj.setNegative (false);
     extract_obj.filter (*prism_content);

     return prism_content;
 }


std::vector <pcl::PointIndices>
normalEstimationAndClustering
(pcl::PointCloud<pcl::PointXYZ>::Ptr prism_content)
{
    // kd-tree object for searches.
    pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);

    kdtree->setInputCloud(prism_content);
    // Estimate the normals.
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normalEstimation;
    normalEstimation.setInputCloud(prism_content);
    normalEstimation.setRadiusSearch(0.03);
    normalEstimation.setSearchMethod(kdtree);
    normalEstimation.compute(*normals);

    // Region growing clustering object.
    pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> clustering;
    std::vector <pcl::PointIndices> clusters;
    clustering.setMinClusterSize(100);
    clustering.setMaxClusterSize(20000);
    clustering.setSearchMethod(kdtree);
    clustering.setNumberOfNeighbours(70);
    clustering.setInputCloud(prism_content);
    clustering.setInputNormals(normals);

    // Set the angle in radians that will be the smoothness threshold
    // (the maximum allowable deviation of the normals).
    clustering.setSmoothnessThreshold(15.0 / 180.0 * M_PI);

    // Set the curvature threshold. The disparity between curvatures will be
    // tested after the normal deviation check has passed.
    clustering.setCurvatureThreshold(1.0);

    clustering.extract(clusters);

    return clusters;
}

Eigen::Vector4f
computeClusterCentroid
(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster)
{
    Eigen::Vector4f cluster_centroid;

    pcl::compute3DCentroid(*filtered_cluster, cluster_centroid);
    std::cout << "Cluster Centroid " << cluster_centroid[0] << " " << cluster_centroid[1] << " " << cluster_centroid[2]
              << " "  << cluster_centroid[3] << std::endl;

    return cluster_centroid;
}

ObjectDetector::Object
computeBoundingBox
(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster,
 Eigen::Vector4f &cluster_centroid)
{
    ObjectDetector::Object object;

    // Compute principal directions
    Eigen::Matrix3f covariance;
    computeCovarianceMatrixNormalized(*filtered_cluster, cluster_centroid, covariance);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
    Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();

    // This line is necessary for proper orientation in some cases. The numbers come out the same without it, but the signs are
    // different and the box doesn't get correctly oriented in some cases.
    //std::cout << "Eigenvectors eigenVectorPCA before :\n" << eigenVectorsPCA << std::endl;
    eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));

    // Transform the original cloud to the origin where the principal components correspond to the axes.
    Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
    projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
    projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * cluster_centroid.head<3>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*filtered_cluster, *cloudPointsProjected, projectionTransform);

    // Get the minimum and maximum points of the transformed cloud.
    // pcl::PointXYZ minPoint, maxPoint;
    pcl::getMinMax3D(*cloudPointsProjected, object.minPoint, object.maxPoint);
    const Eigen::Vector3f meanDiagonal = 0.5f*(object.maxPoint.getVector3fMap() + object.minPoint.getVector3fMap());

    std::cout << "minPoint: " << object.minPoint << "; maxPoint:  " << object.maxPoint << " " << std::endl;
    std::cout << "meanDiagonal:" << meanDiagonal << std::endl;

    // Final transform
    const Eigen::Quaternionf bboxQuaternion_temp(eigenVectorsPCA);
    object.bboxQuaternion = bboxQuaternion_temp;
    // Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
    //                      3x1               3x3              3x1              4x1
    // bboxTransform ist der Mittelpunkt der Box um das jeweilige Cluster
    const Eigen::Vector3f bboxTransform_temp = eigenVectorsPCA * meanDiagonal + cluster_centroid.head<3>();
    object.bboxTransform = bboxTransform_temp;

    return object;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
copyAndFilterCluster
(pcl::PointCloud<pcl::PointXYZ>::Ptr working_cluster){

    working_cluster->width = working_cluster->points.size();
    working_cluster->height = 1;
    working_cluster->is_dense = true;

    //Filtering
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> statFilter;
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster(new pcl::PointCloud<pcl::PointXYZ>);
    statFilter.setInputCloud(working_cluster);
    statFilter.setMeanK(50);
    statFilter.setStddevMulThresh(1.0);
    statFilter.filter(*filtered_cluster);
    return filtered_cluster;
}
} //end namespace
#endif
