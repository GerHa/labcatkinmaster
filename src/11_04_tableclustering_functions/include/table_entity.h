
#include <pcl/visualization/eigen.h>
#include <pcl/point_types.h>
#include "ros_simplyfier.h"

#ifndef _TABLE_ENTITY_H
#define _TABLE_ENTITY_H

namespace TableDetector {
struct TableEntity {
    Eigen::Vector3f bboxTransform;
    Eigen::Quaternionf bboxQuaternion;
    pcl::PointXYZ minPoint;
    pcl::PointXYZ maxPoint;
    pointXYZ_PTR cloud;
};
}
#endif
