#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <tf/transform_broadcaster.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "objects.h"

#ifndef _OBJECT_DETECTOR
#define _OBJECT_DETECTOR

namespace ObjectDetector {
class Detector {
public:
    Detector();
    ~Detector();

    Objects
    get_objects_from_point_cloud
    ( const sensor_msgs::PointCloud2 & );

    void
    get_information
    (const sensor_msgs::PointCloud2 &,
     pcl::visualization::PCLVisualizer *);

    void go();

    bool
    any_key_pressed();
};
} // namespace end
#endif
