
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>


#ifndef _TRANSFORMATIONS
#define _TRANSFORMATIONS
namespace transformations {
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    convertPC2toPxyz
    (const sensor_msgs::PointCloud2 &input);
}
#endif
