
#include <pcl/point_cloud.h>

#ifndef _ROS_SIMPLFIER
#define _ROS_SIMPLFIER

typedef pcl::PointCloud<pcl::PointXYZ>::Ptr pointXYZ_PTR;
typedef pcl::PointCloud<pcl::PointXYZ> pointXYZ_CLOUD;

#endif
