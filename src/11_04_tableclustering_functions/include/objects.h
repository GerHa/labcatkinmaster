
#include <pcl/visualization/eigen.h>
#include <pcl/point_types.h>
#include "ros_simplyfier.h"

#ifndef _OBJECT_ENTITY_H
#define _OBJECT_ENTITY_H

namespace ObjectDetector {
struct Object {
    Eigen::Vector3f bboxTransform;
    Eigen::Quaternionf bboxQuaternion;
    pcl::PointXYZ minPoint;
    pcl::PointXYZ maxPoint;
    pointXYZ_PTR cloud;
    pointXYZ_PTR table;
};

typedef std::vector<Object> Objects;
}
#endif
