/**
 * @file /include/RobotHeadInterface/qnode.hpp
 *
 * @brief Communications central!
 *
 * @date February 2011
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef RobotHeadInterface_QNODE_HPP_
#define RobotHeadInterface_QNODE_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include <std_msgs/String.h>
#include <QThread>
#include <QStringListModel>
#include <sensor_msgs/PointCloud2.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace RobotHeadInterface {

/*****************************************************************************
** Class
*****************************************************************************/

class QNode : public QThread {
    Q_OBJECT
public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	bool init();
	bool init(const std::string &master_url, const std::string &host_url);
	void run();
    void cloud_cb(const sensor_msgs::PointCloud2ConstPtr& input);

public Q_SLOTS:

Q_SIGNALS:
    void rosShutdown();


private:
	int init_argc;
    char** init_argv;
    ros::Publisher pub;
    ros::Subscriber sub;
};

}  // namespace RobotHeadInterface

#endif /* RobotHeadInterface_QNODE_HPP_ */
