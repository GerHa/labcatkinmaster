/**
 * @file /src/main_window.cpp
 *
 * @brief Implementation for the qt gui.
 *
 * @date February 2011
 **/
/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "../include/RobotHeadInterface/main_window.hpp"
#include <geometry_msgs/TransformStamped.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace RobotHeadInterface {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
    // Calling this incidentally connects all ui's 
    // triggers to on_...() callbacks in this class.
	ui.setupUi(this);
    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));
}

MainWindow::~MainWindow() {}
void MainWindow::on_button_connect_clicked(bool check ) {
    if ( ui.checkbox_use_environment->isChecked() ) {
        if ( !qnode.init() ) {
            qDebug() << "not connected";
        } else {
            qDebug()<<"connected";
        }
    }
}

/*****************************************************************************
** Implementation [Configuration]
*****************************************************************************/

void MainWindow::ReadSettings() {
    QSettings settings("Qt-Ros Package", "RobotHeadInterface");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
    QString master_url = settings.value("master_url",QString("http://192.168.1.2:11311/")).toString();
    QString host_url = settings.value("host_url", QString("192.168.1.3")).toString();
    //QString topic_name = settings.value("topic_name", QString("/chatter")).toString();
    ui.line_edit_master->setText(master_url);
    ui.line_edit_host->setText(host_url);
    //ui.line_edit_topic->setText(topic_name);
    bool remember = settings.value("remember_settings", false).toBool();
    ui.checkbox_remember_settings->setChecked(remember);
    bool checked = settings.value("use_environment_variables", false).toBool();
    ui.checkbox_use_environment->setChecked(checked);
    if ( checked ) {
    	ui.line_edit_master->setEnabled(false);
    	ui.line_edit_host->setEnabled(false);
    	//ui.line_edit_topic->setEnabled(false);
    }
}

void MainWindow::WriteSettings() {
    QSettings settings("Qt-Ros Package", "RobotHeadInterface");
    settings.setValue("master_url",ui.line_edit_master->text());
    settings.setValue("host_url",ui.line_edit_host->text());
    //settings.setValue("topic_name",ui.line_edit_topic->text());
    settings.setValue("use_environment_variables",QVariant(ui.checkbox_use_environment->isChecked()));
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    settings.setValue("remember_settings",QVariant(ui.checkbox_remember_settings->isChecked()));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	WriteSettings();
	QMainWindow::closeEvent(event);
}

}  // namespace RobotHeadInterface

/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <QMessageBox>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include "../include/RobotHeadInterface/qnode.hpp"

#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>



/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace RobotHeadInterface {

/*****************************************************************************
** Implementation
*****************************************************************************/

QNode::QNode(int argc, char** argv ) :               // Constructor for QNode
    init_argc(argc),
    init_argv(argv)
{}

QNode::~QNode() {                                    // Destructor for QNode
    if(ros::isStarted()) {

        // explicitly needed since we use ros::start();
        ros::shutdown();
        ros::waitForShutdown();
    }
    wait();
}
// Function initialize of QNode when called with no input
// This function starts and creates a Node.
// The types of the messages are defined in this function
bool QNode::init() {

    // initialize ros with no specific input arguments and name 'RobotHeadInterface'
    ros::init(init_argc,init_argv,"RobotHeadInterface");
    //If ros::master not online, initialize is false
    if ( ! ros::master::check() ) {
        return false;
    }

    // explicitly needed since our nodehandle is going out of scope
    ros::start();
    // Define nh as NodeHandle.
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();


    // Create a ROS subscriber for the input point cloud
    sub = nh.subscribe ("kinect2/sd/points", 100,&QNode::cloud_cb,this);


    // Create a ROS publisher for the output point cloud
    pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 100);

    ros::spin();
    // Init was sucessfull
    return true;
}

// Function run() of QNode.
void QNode::run() {
    ros::Rate loop_rate(1000);

    while ( ros::ok() ) {
    }
    // used to signal the gui for a shutdown (useful to roslaunch)
    Q_EMIT rosShutdown();
}

void QNode::cloud_cb(const sensor_msgs::PointCloud2ConstPtr& input)
{


    // Create a container for the data.
    sensor_msgs::PointCloud2 output;

    // Do data processing here...
    output = *input;

    // Publish the data.
    //pub.publish (output);
}
}  // namespace RobotHeadInterface
