/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <QMessageBox>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include "../include/RobotHeadInterface/qnode.hpp"

#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>



/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace RobotHeadInterface {

/*****************************************************************************
** Implementation
*****************************************************************************/

QNode::QNode(int argc, char** argv ) :               // Constructor for QNode
    init_argc(argc),
    init_argv(argv)
{}

QNode::~QNode() {                                    // Destructor for QNode
    if(ros::isStarted()) {

        // explicitly needed since we use ros::start();
        ros::shutdown();
        ros::waitForShutdown();
    }
    wait();
}
// Function initialize of QNode when called with no input
// This function starts and creates a Node.
// The types of the messages are defined in this function
bool QNode::init() {

    // initialize ros with no specific input arguments and name 'RobotHeadInterface'
    ros::init(init_argc,init_argv,"RobotHeadInterface");
    //If ros::master not online, initialize is false
    if ( ! ros::master::check() ) {
        return false;
    }

    // explicitly needed since our nodehandle is going out of scope
    ros::start();
    // Define nh as NodeHandle.
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();


    // Create a ROS subscriber for the input point cloud
    sub = nh.subscribe ("kinect2/sd/points", 100,&QNode::cloud_cb,this);


    // Create a ROS publisher for the output point cloud
    pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 100);

    ros::spin();
    // Init was sucessfull
    return true;
}

// Function run() of QNode.
void QNode::run() {
    ros::Rate loop_rate(1000);

    while ( ros::ok() ) {
    }
    // used to signal the gui for a shutdown (useful to roslaunch)
    Q_EMIT rosShutdown();
}

void QNode::cloud_cb(const sensor_msgs::PointCloud2ConstPtr& input)
{


    // Create a container for the data.
    sensor_msgs::PointCloud2 output;

    // Do data processing here...
    output = *input;

    // Publish the data.
    //pub.publish (output);
}
}  // namespace RobotHeadInterface
