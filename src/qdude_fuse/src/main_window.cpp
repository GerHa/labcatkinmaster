/**
 * @file /src/main_window.cpp
 *
 * @brief Implementation for the qt gui.
 *
 * @date February 2011
 **/
/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "../include/qdude_fuse/main_window.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace qdude {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
    qnode.init();

	ui.setupUi(this); // Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.

	setWindowIcon(QIcon(":/images/icon.png"));

    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

    /*********************
    ** Auto Start
    **********************/
    if ( ui.checkbox_remember_settings->isChecked() ) {
        on_button_connect_clicked(true);
    }


}

MainWindow::~MainWindow() {}

/*****************************************************************************
** Implementation [Slots]
*****************************************************************************/

void MainWindow::showNoMasterMessage() {
	QMessageBox msgBox;
	msgBox.setText("Couldn't find the ros master.");
	msgBox.exec();
    close();
}

/*
 * These triggers whenever the button is clicked, regardless of whether it
 * is already checked or not.
 */

void MainWindow::on_button_connect_clicked(bool check ) {
    if ( !qnode.init() ) {
        qDebug() << "hi";
        showNoMasterMessage();
    }
}

void MainWindow::on_pushButton_clicked(){
    qDebug() << "hi";


}

void MainWindow::on_BttnStartTableDetection_clicked(){
    qDebug() << "on_BttnStartTableDetection_clicked";
    detector = new TableDetector();
    detector->start();



}

void MainWindow::on_BttnStopTableDetection_clicked(){
    qDebug() << "on_BttnStopTableDetection_clicked";

    std::cout << "maxPoint :" << detector->DetectedTableGeometry.maxPoint << std::endl;
    std::cout << "minPoint :" << detector->DetectedTableGeometry.minPoint << std::endl;

}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QMainWindow::closeEvent(event);
}

}
