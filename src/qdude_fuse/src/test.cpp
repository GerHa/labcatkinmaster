#include "../include/qdude_fuse/test.hpp"

#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <tf/transform_broadcaster.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/range_image/range_image.h>
#include <pcl/common/transforms.h>
#include <pcl/common/pca.h>




PointcloudProcessing::PointcloudProcessing()
{
    std::cout << "PointcloudProcessing() called" << std::endl;

}

void  PointcloudProcessing::tableDetectionTopic() {
    ros::NodeHandle nh;
    std::cout << "TableDetectionTopic() called" << std::endl;
    detectTableSubscriber = nh.subscribe("kinect2/sd/points", 5, &PointcloudProcessing::tableDetection, this);
}

void  PointcloudProcessing::subscribeToTopic() {
    ros::NodeHandle nh;
    std::cout << "subscribeToTopic() called" << std::endl;
    pcl_sub = nh.subscribe("kinect2/sd/points", 5, &PointcloudProcessing::objectDetection, this);
    //timer = nh.createTimer(ros::Duration(0.1), &PointcloudProcessing::timerfunc, this);
      std::cout << "test" << std::endl;
}

void  PointcloudProcessing::tableDetection(const sensor_msgs::PointCloud2 &input){

//    ros::NodeHandle nh;
//    pcl_pub.publish(input);
//           pcl_pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);
    std::cout << "tableDetection() called" << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered = PointcloudProcessing::convertPC2toPxyz(input);
//    std::cout << cloud_filtered->width * cloud_filtered->height << std::endl;

}

void  PointcloudProcessing::objectDetection(const sensor_msgs::PointCloud2 &input){

//    ros::NodeHandle nh;
//    pcl_pub.publish(input);
//           pcl_pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered = PointcloudProcessing::convertPC2toPxyz(input);
//    std::cout << cloud_filtered->width * cloud_filtered->height << std::endl;

         std::cout << "subscribed" << std::endl;
}

// Convert the PointCloud format
//pcl::PointCloud<pcl::PointXYZ>::Ptr
pcl::PointCloud<pcl::PointXYZ>::Ptr PointcloudProcessing::convertPC2toPxyz(const sensor_msgs::PointCloud2 &input){

    // Convert to the templated PointCloud
    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2);
    pcl_conversions::toPCL(input, *cloud_blob);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_temp (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_filtered_temp);

    return cloud_filtered_temp;
}

// shift angle and pull cloud towarsa origin of the coordinate system
pcl::PointCloud<pcl::PointXYZ>::Ptr transformStartCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered){

    // # # Transformation # #
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    // Define a translation of 2.5 meters on the x axis.
    transform_2.translation() << 0.0, 0.0, -0.65;
    // The same rotation matrix as before; tetha radians arround Z axis
    transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
    // Print the transformation
    // printf ("\nMethod #2: using an Affine3f\n");
    //std::cout << transform_2.matrix() << std::endl;
    // Executing the transformation
    // You can either apply transform_1 or transform_2; they are the same
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_temp (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud (*cloud_filtered, *transformed_cloud_temp, transform_2);

    return transformed_cloud_temp;
}

//Filter on the z axis, filters out the wall in the back of the point cloud
pcl::PointCloud<pcl::PointXYZ>::Ptr passThroughZ(pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud){

    //      PassThrough filter for z-axis
    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_zrange.setInputCloud(transformed_cloud);
    filter_zrange.setFilterFieldName("z");
    filter_zrange.setFilterLimits(0.0, 0.95);
    filter_zrange.filter(*zrange_filteredCloud);
    //std::cerr << "zRangeFiltered: " << zrange_filteredCloud->width * zrange_filteredCloud->height << " data points." << std::endl;

    return zrange_filteredCloud;
}

// Filter on the y axis, filters out the ground
pcl::PointCloud<pcl::PointXYZ>::Ptr passThroughY (pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud){

    //   PassThrough filter for y-axis
    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_yrange.setInputCloud(zrange_filteredCloud);
    filter_yrange.setFilterFieldName("y");
    filter_yrange.setFilterLimits(-2.0, 0.7);
    filter_yrange.filter(*yzrange_filteredCloud);

//    std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points."
//              << std::endl;
    return yzrange_filteredCloud;
}

//extract the table plane and its negative to remove it from the original cloud
void planeSegmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, pcl::ModelCoefficients::Ptr coefficients,  pcl::PointIndices::Ptr inliers,  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f){

    // # # SEGMENTATION # #
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);
    //determines how close a point must be to the model in order to be considered an inlier
    seg.setDistanceThreshold (0.01);

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;

    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (yzrange_filteredCloud);
    seg.segment (*inliers, *coefficients);

    //Extract the inliers
    extract.setInputCloud (yzrange_filteredCloud);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*cloud_p);
//    std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points."
//              << std::endl;

    // Write ervery plane to a seperate pcd file
    // std::stringstream ss;
    // ss << "../../../../../plane" << i << ".pcd";
    // ss << "table_scene_lms400_plane_" << i << ".pcd";
    //  pcl::PCDWriter writer;
    //  writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

    // Create the filtering object
     extract.setNegative (true);
     extract.filter (*cloud_f);

    // i++;
    // writer.write("../../../../../filtered_and_downsampled_cloud.pcd", *cloud_filtered);

}

// compute the hull of the table plane and a prisma above this hull, the content of this prisma is our new point cloud
// with everything that is on the table
pcl::PointCloud<pcl::PointXYZ>::Ptr computePlaneHullAndPrism (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud){

     // getting the hull of the found plane
     pcl::PointIndices::Ptr cloud_indices (new pcl::PointIndices ());
     double z_min = 0., z_max =0.5;  // points above the plane with maximum height z_max
     pcl::PointCloud<pcl::PointXYZ>::Ptr hull_points (new pcl::PointCloud<pcl::PointXYZ> ());
     pcl::ConvexHull<pcl::PointXYZ> hull;
     // hull.setDimension (2); // not necessarily needed, but we need to check the dimensionality of the output
     hull.setInputCloud(cloud_p);
     //hull.setAlpha(0.1);
     hull.reconstruct (*hull_points);
     if (hull.getDimension() == 2)
     {
         pcl::ExtractPolygonalPrismData<pcl::PointXYZ> prism;
         prism.setInputCloud(yzrange_filteredCloud);
         prism.setInputPlanarHull(hull_points);
         prism.setHeightLimits(z_min, z_max);
         prism.segment(*cloud_indices);
     }
     // extract the indices above the plane from ititial cloud into a separate one
     pcl::ExtractIndices<pcl::PointXYZ> extract_obj;
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o (new pcl::PointCloud<pcl::PointXYZ>);
     extract_obj.setInputCloud (yzrange_filteredCloud);
     extract_obj.setIndices (cloud_indices);
     extract_obj.setNegative (false);
     extract_obj.filter (*cloud_o);

     // //to vizualize the computed hull
     //  pcl::visualization::CloudViewer viewerplane ("Convex Hull");
     //  viewerplane.showCloud(hull_points);
     //  while (!viewerplane.wasStopped())
     //  {
     //  // Do nothing but wait.
     //  }
     //double start = clock();
     //pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
     //ne.setInputCloud(cloud_o);
     //pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
     // // Use all neighbors in a sphere of radius 3cm
     //ne.setRadiusSearch(0.03);
     //ne.compute(*cloud_normals);
     //double finish =clock();

     //double total_time = (double)(finish - start)/CLOCKS_PER_SEC;
     //cout << "normal estimation TIME=" <<total_time << endl;
     return cloud_o;
 }


//cluster the related points/objects on the table
std::vector <pcl::PointIndices> normalEstimationAndClustering(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o){

    // kd-tree object for searches.
    pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);

    kdtree->setInputCloud(cloud_o);
    // Estimate the normals.
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normalEstimation;
    normalEstimation.setInputCloud(cloud_o);
    normalEstimation.setRadiusSearch(0.03);
    normalEstimation.setSearchMethod(kdtree);
    normalEstimation.compute(*normals);

    // Region growing clustering object.
    pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> clustering;
    std::vector <pcl::PointIndices> clusters;
    clustering.setMinClusterSize(100);
    clustering.setMaxClusterSize(20000);
    clustering.setSearchMethod(kdtree);
    clustering.setNumberOfNeighbours(70);
    clustering.setInputCloud(cloud_o);
    clustering.setInputNormals(normals);
    // Set the angle in radians that will be the smoothness threshold
    // (the maximum allowable deviation of the normals).
    clustering.setSmoothnessThreshold(15.0 / 180.0 * M_PI); // 7 degrees.
    // Set the curvature threshold. The disparity between curvatures will be
    // tested after the normal deviation check has passed.
    clustering.setCurvatureThreshold(1.0);
    //std::vector <pcl::PointIndices> clusters;
    clustering.extract(clusters);

    /*
     * http://stackoverflow.com/questions/29938866/pcl-how-to-create-a-point-cloud-array-vector
     */

    // ...and save it to disk.
    //    if (cluster->points.size() <= 0)
    //       break;
    //    std::cout << "Cluster " << currentClusterNum << " has " << cluster->points.size() << " points." << std::endl;
    //    std::string fileName = "cluster" + boost::to_string(currentClusterNum) + ".pcd";
    //    pcl::io::savePCDFileASCII(fileName, *cluster);
    return clusters;
}

// copy the cluster into a vector, filter the clusters statistical outliers
pcl::PointCloud<pcl::PointXYZ>::Ptr copyAndFilterCluster( pcl::PointCloud<pcl::PointXYZ>::Ptr working_cluster){

    std::vector < pcl::PointCloud<pcl::PointXYZ>::Ptr, Eigen::aligned_allocator <pcl::PointCloud <pcl::PointXYZ>::Ptr > > sourceClouds;
    working_cluster->width = working_cluster->points.size();
    working_cluster->height = 1;
    working_cluster->is_dense = true;

    //Filtering
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> statFilter;
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster(new pcl::PointCloud<pcl::PointXYZ>);
    statFilter.setInputCloud(working_cluster);
    statFilter.setMeanK(50);
    statFilter.setStddevMulThresh(1.0);
    statFilter.filter(*filtered_cluster);
    sourceClouds.push_back(filtered_cluster);
    return filtered_cluster;
}

// compute the clusters centroid
Eigen::Vector4f computeClusterCentroid(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster){

    // # # # Computing the centroid of the cluster # # #
    Eigen::Vector4f cluster_centroid;
    pcl::compute3DCentroid(*filtered_cluster, cluster_centroid);
    //    pcl::CentroidPoint<pcl::PointXYZ> table_centroid;
    //    table_centroid.add (pcl::PointCloud (cloud_p);
    //    pcl::PointXYZ c1;
    //    table_centroid.get(c1);
    //std::cout << "Cluster Centroid " << cluster_centroid[0] << " " << cluster_centroid[1] << " " << cluster_centroid[2]
              //<< " "  << cluster_centroid[3] << std::endl;
    return cluster_centroid;
}


struct mytype{
     Eigen::Quaternionf bboxQuaternion;
     Eigen::Vector3f bboxTransform;
     pcl::PointXYZ minPoint, maxPoint;
};
// compute the bounding boxes of the individual clusters
mytype computeBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster, Eigen::Vector4f &cluster_centroid){

    // Compute principal directions
    Eigen::Matrix3f covariance;
    computeCovarianceMatrixNormalized(*filtered_cluster, cluster_centroid, covariance);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
    Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
    // This line is necessary for proper orientation in some cases. The numbers come out the same without it, but the signs are
    // different and the box doesn't get correctly oriented in some cases.
    //std::cout << "Eigenvectors eigenVectorPCA before :\n" << eigenVectorsPCA << std::endl;
    eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));

    // Transform the original cloud to the origin where the principal components correspond to the axes.
    Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
    projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
    projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * cluster_centroid.head<3>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*filtered_cluster, *cloudPointsProjected, projectionTransform);
    // Get the minimum and maximum points of the transformed cloud.
    //pcl::PointXYZ minPoint, maxPoint;
    mytype Box;
    pcl::getMinMax3D(*cloudPointsProjected, Box.minPoint, Box.maxPoint);
    const Eigen::Vector3f meanDiagonal = 0.5f*(Box.maxPoint.getVector3fMap() + Box.minPoint.getVector3fMap());

    //std::cout << "minPoint: " << Box.minPoint << "; maxPoint:  " << Box.maxPoint << " " << std::endl;
    //std::cout << "meanDiagonal:" << meanDiagonal << std::endl;

    // Final transform
    const Eigen::Quaternionf bboxQuaternion_temp(eigenVectorsPCA);

    Box.bboxQuaternion = bboxQuaternion_temp;
    //const Eigen::Quaternionf bboxQuaternion (eigenVectorsPCA);
    // Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
    //                      3x1               3x3              3x1              4x1
    // bboxTransform ist der Mittelpunkt der Box um das jeweilige Cluster
    Box.bboxTransform = eigenVectorsPCA * meanDiagonal + cluster_centroid.head<3>();
    //const Eigen::Vector3f bboxTransform = eigenVectorsPCA * meanDiagonal + cluster_centroid.head<3>();

     return Box;
}

