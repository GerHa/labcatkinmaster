#include "../include/qdude_fuse/TableDetector.h"
#include <ros/ros.h>
#include <pcl/ModelCoefficients.h>
//#include "tableclustering_functions/cHandler_tableclustering_functions.h"
//#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/filters/extract_indices.h>
//#include <pcl/visualization/cloud_viewer.h>
#include <pcl_conversions/pcl_conversions.h>
//#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
//#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>
//#include <tf/transform_broadcaster.h>
#include <pcl/filters/passthrough.h>
//#include <pcl/segmentation/extract_polygonal_prism_data.h>
//#include <pcl/surface/convex_hull.h>
//#include <pcl/surface/concave_hull.h>
#include <pcl/segmentation/extract_clusters.h>
//#include <pcl/features/integral_image_normal.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>
#include <pcl/segmentation/region_growing.h>
//#include <pcl/range_image/range_image.h>
#include <pcl/common/transforms.h>
#include <pcl/common/pca.h>

TableDetector::TableDetector()
    : viewer("Cloud Viewer")
{
    viewer.addCoordinateSystem(1.0,0);

}

void TableDetector::run()
{

    pcl_sub = nh.subscribe("kinect2/sd/points",5, &TableDetector::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &TableDetector::timerCB, this);

    ros::NodeHandle node;

    ros::Rate rate(1);
    while (node.ok()){
        while (ros::ok())
        {
            ros::spinOnce();
            rate.sleep();
        }
    }
}

void
TableDetector::cloudCB
(const sensor_msgs::PointCloud2 &input)
{
    std::cout << "cloudCB() called" << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered = TableDetector::convertPC2toPxyz(input);

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud = TableDetector::transformStartCloud(cloud_filtered);

    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud = TableDetector::passThroughZ(transformed_cloud);

    std::cout << "passThroughZ() done" << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud =TableDetector::passThroughY(zrange_filteredCloud);

    std::cout << "passThroughY() done" << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p =TableDetector::planeSegmentation(yzrange_filteredCloud);

    std::cout << "planeSegmentation() done" << std::endl;

    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    std::vector <pcl::PointIndices> clusters;

    std::cout << "normalEstimationAndClustering() done" << std::endl;

    Eigen::Vector4f cluster_centroid;
    this->computeClusterCentroid(cloud_p, cluster_centroid);

    std::cout << "computeClusterCentroid() done" << std::endl;

    Eigen::Vector3f bboxTransform;
    Eigen::Quaternionf bboxQuaternion;
    pcl::PointXYZ minPoint, maxPoint;
    this->computeBoundingBox(cloud_p, cluster_centroid, bboxTransform, bboxQuaternion, minPoint, maxPoint);

    std::cout << "computeBoundingBox() done" << std::endl;

    DetectedTableGeometry.bboxTransform = bboxTransform;
    DetectedTableGeometry.bboxQuaternion = bboxQuaternion;
    DetectedTableGeometry.maxPoint = maxPoint;
    DetectedTableGeometry.minPoint = minPoint;

    viewer.removeAllPointClouds();
    viewer.addPointCloud(cloud_p);
    viewer.addCube(bboxTransform, bboxQuaternion, maxPoint.x - minPoint.x, maxPoint.y - minPoint.y, maxPoint.z - minPoint.z);
    viewer.addCoordinateSystem(1,bboxTransform[0], bboxTransform[1], bboxTransform[2]);
    std::cout << "finish()" << std::endl;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
TableDetector::convertPC2toPxyz
(const sensor_msgs::PointCloud2 &input){

        // Convert to the templated PointCloud
        pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2);
        pcl_conversions::toPCL(input, *cloud_blob);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_temp (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::fromPCLPointCloud2 (*cloud_blob, *cloud_filtered_temp);

        return cloud_filtered_temp;
}


pcl::PointCloud<pcl::PointXYZ>::Ptr
TableDetector::transformStartCloud
(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered)
{

    // # # Transformation # #
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    // Define a translation of 2.5 meters on the x axis.
    transform_2.translation() << 0.0, 0.0, -0.65;
    // The same rotation matrix as before; tetha radians arround Z axis
    transform_2.rotate (Eigen::AngleAxisf (-0.48869219055841, Eigen::Vector3f::UnitX())); //20deg
    // Print the transformation
    // printf ("\nMethod #2: using an Affine3f\n");
    //std::cout << transform_2.matrix() << std::endl;
    // Executing the transformation
    // You can either apply transform_1 or transform_2; they are the same
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_temp (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud (*cloud_filtered, *transformed_cloud_temp, transform_2);

    return transformed_cloud_temp;
}


pcl::PointCloud<pcl::PointXYZ>::Ptr
TableDetector::passThroughZ
(pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud)
{

    //      PassThrough filter for z-axis
    pcl::PassThrough<pcl::PointXYZ> filter_zrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_zrange.setInputCloud(transformed_cloud);
    filter_zrange.setFilterFieldName("z");
    filter_zrange.setFilterLimits(0.0, 0.95);
    filter_zrange.filter(*zrange_filteredCloud);
    std::cerr << "zRangeFiltered: " << zrange_filteredCloud->width * zrange_filteredCloud->height << " data points." << std::endl;

    return zrange_filteredCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
TableDetector::passThroughY
(pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud)
{

    //   PassThrough filter for y-axis
    pcl::PassThrough<pcl::PointXYZ> filter_yrange;
    pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud (new pcl::PointCloud<pcl::PointXYZ>);
    filter_yrange.setInputCloud(zrange_filteredCloud);
    filter_yrange.setFilterFieldName("y");
    filter_yrange.setFilterLimits(-2.0, 0.7);
    filter_yrange.filter(*yzrange_filteredCloud);

//    std::cerr << "PointCloud after PassThrough: " << yzrange_filteredCloud->width * yzrange_filteredCloud->height << " data points."
//              << std::endl;
    return yzrange_filteredCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr
TableDetector::planeSegmentation
(pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud)
{

    // # # SEGMENTATION # #
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);
    //determines how close a point must be to the model in order to be considered an inlier
    seg.setDistanceThreshold (0.01);

    // Create the filtering object
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
     pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (yzrange_filteredCloud);
    seg.segment (*inliers, *coefficients);

    //Extract the inliers
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p (new pcl::PointCloud<pcl::PointXYZ>);
    extract.setInputCloud (yzrange_filteredCloud);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*cloud_p);
//    std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points."
//              << std::endl;

    // Write ervery plane to a seperate pcd file
    // std::stringstream ss;
    // ss << "../../../../../plane" << i << ".pcd";
    // ss << "table_scene_lms400_plane_" << i << ".pcd";
    //  pcl::PCDWriter writer;
    //  writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

    // Create the filtering object
    // extract.setNegative (true);
    // extract.filter (*cloud_f);
    // yzrange_filteredCloud.swap (cloud_f);
    // writer.write("../../../../../filtered_and_downsampled_cloud.pcd", *cloud_filtered);
    return cloud_p;
}

void
TableDetector::normalEstimationAndClustering
(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,  pcl::PointCloud<pcl::Normal>::Ptr normals, std::vector <pcl::PointIndices> &clusters)
{

    // kd-tree object for searches.
    pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);

    kdtree->setInputCloud(cloud_p);

    // Estimate the normals.
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normalEstimation;
    normalEstimation.setInputCloud(cloud_p);
    normalEstimation.setRadiusSearch(0.03);
    normalEstimation.setSearchMethod(kdtree);
    normalEstimation.compute(*normals);

    std::cout << "normals() done" << std::endl;

    // Region growing clustering object.
    pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> clustering;
    clustering.setMinClusterSize(100);
    clustering.setMaxClusterSize(20000);
    clustering.setSearchMethod(kdtree);
    clustering.setNumberOfNeighbours(70);
    clustering.setInputCloud(cloud_p);
    clustering.setInputNormals(normals);

    std::cout << "clustering() done" << std::endl;


    // Set the angle in radians that will be the smoothness threshold
    // (the maximum allowable deviation of the normals).
    clustering.setSmoothnessThreshold(15.0 / 180.0 * M_PI); // 7 degrees.
    // Set the curvature threshold. The disparity between curvatures will be
    // tested after the normal deviation check has passed.
    clustering.setCurvatureThreshold(1.0);
    //std::vector <pcl::PointIndices> clusters;
    clustering.extract(clusters);

    std::cout << "extract() done" << std::endl;

    /*
     * http://stackoverflow.com/questions/29938866/pcl-how-to-create-a-point-cloud-array-vector
     */

    // ...and save it to disk.
    //    if (cluster->points.size() <= 0)
    //       break;
    //    std::cout << "Cluster " << currentClusterNum << " has " << cluster->points.size() << " points." << std::endl;
    //    std::string fileName = "cluster" + boost::to_string(currentClusterNum) + ".pcd";
    //    pcl::io::savePCDFileASCII(fileName, *cluster);

}

void
TableDetector::computeClusterCentroid
(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, Eigen::Vector4f &cluster_centroid)
{

    // # # # Computing the centroid of the cluster # # #
    pcl::compute3DCentroid(*cloud_p, cluster_centroid);
    //    pcl::CentroidPoint<pcl::PointXYZ> table_centroid;
    //    table_centroid.add (pcl::PointCloud (cloud_p);
    //    pcl::PointXYZ c1;
    //    table_centroid.get(c1);
    std::cout << "Cluster Centroid " << cluster_centroid[0] << " " << cluster_centroid[1] << " " << cluster_centroid[2]
              << " "  << cluster_centroid[3] << std::endl;

}

void
TableDetector::computeBoundingBox
(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p, Eigen::Vector4f &cluster_centroid, Eigen::Vector3f &bboxTransform, Eigen::Quaternionf &bboxQuaternion, pcl::PointXYZ &minPoint, pcl::PointXYZ &maxPoint)
{

    // Compute principal directions
    Eigen::Matrix3f covariance;
    computeCovarianceMatrixNormalized(*cloud_p, cluster_centroid, covariance);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
    Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
    // This line is necessary for proper orientation in some cases. The numbers come out the same without it, but the signs are
    // different and the box doesn't get correctly oriented in some cases.
    //std::cout << "Eigenvectors eigenVectorPCA before :\n" << eigenVectorsPCA << std::endl;
    eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));

    //        std::cout << "Eigenvectors eigenVectorPCA:\n" << eigenVectorsPCA << std::endl;
    //        // Note that getting the eigenvectors can also be obtained via the PCL PCA interface with something like:
    //        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPCAprojection (new pcl::PointCloud<pcl::PointXYZ>);
    //        pcl::PCA<pcl::PointXYZ> pca;
    //        pca.setInputCloud(filtered_cluster);
    //        pca.project(*filtered_cluster, *cloudPCAprojection);
    //        std::cout << std::endl << "EigenVectors: " << pca.getEigenVectors() << std::endl;
    //        std::cerr << std::endl << "EigenValues: " << pca.getEigenValues() << std::endl;
    //        // In this case, pca.getEigenVectors() gives similar eigenVectors to eigenVectorsPCA.


    // Transform the original cloud to the origin where the principal components correspond to the axes.
    Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
    projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
    projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * cluster_centroid.head<3>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*cloud_p, *cloudPointsProjected, projectionTransform);
    // Get the minimum and maximum points of the transformed cloud.
    // pcl::PointXYZ minPoint, maxPoint;
    pcl::getMinMax3D(*cloudPointsProjected, minPoint, maxPoint);
    const Eigen::Vector3f meanDiagonal = 0.5f*(maxPoint.getVector3fMap() + minPoint.getVector3fMap());

    std::cout << "minPoint: " << minPoint << "; maxPoint:  " << maxPoint << " " << std::endl;
    std::cout << "meanDiagonal:" << meanDiagonal << std::endl;

    // Final transform
    const Eigen::Quaternionf bboxQuaternion_temp(eigenVectorsPCA);
    bboxQuaternion = bboxQuaternion_temp;
    // Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
    //                      3x1               3x3              3x1              4x1
    // bboxTransform ist der Mittelpunkt der Box um das jeweilige Cluster
    const Eigen::Vector3f bboxTransform_temp = eigenVectorsPCA * meanDiagonal + cluster_centroid.head<3>();
    bboxTransform = bboxTransform_temp;

}

void
TableDetector::timerCB
(const ros::TimerEvent&)
{
    std::cout << "timerCB called" << std::cout;
    viewer.spinOnce ();
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
