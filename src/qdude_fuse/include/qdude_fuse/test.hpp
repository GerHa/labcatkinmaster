
/*****************************************************************************
** Includes
*****************************************************************************/


#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>


class PointcloudProcessing {
private:
    pcl::PointXYZ TablePosition;
    void tableDetection(const sensor_msgs::PointCloud2 &input);

public:
    PointcloudProcessing();

    ros::Subscriber pcl_sub;
    ros::Subscriber detectTableSubscriber;
    ros::Publisher pcl_pub;

    ros::Timer timer;
    void timerfunc(const ros::TimerEvent&);
    void objectDetection(const sensor_msgs::PointCloud2 &input);

    void tableDetectionTopic();
    void subscribeToTopic();

    pcl::PointCloud<pcl::PointXYZ>::Ptr
    convertPC2toPxyz
    (const sensor_msgs::PointCloud2 &input);

    //void convertPC2toPxyz(const sensor_msgs::PointCloud2 &input);

    pcl::PointCloud<pcl::PointXYZ>::Ptr
    transformStartCloud
    (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered);

    pcl::PointCloud<pcl::PointXYZ>::Ptr
    passThroughZ
    (pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud);

    pcl::PointCloud<pcl::PointXYZ>::Ptr
    passThroughY
    (pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud);

    void
    planeSegmentation
    (pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud,
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,
     pcl::ModelCoefficients::Ptr coefficients,
     pcl::PointIndices::Ptr inliers,
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f);

    pcl::PointCloud<pcl::PointXYZ>::Ptr
    computePlaneHullAndPrism
    (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,
     pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud);

    std::vector <pcl::PointIndices>
    normalEstimationAndClustering
    (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_o);

    pcl::PointCloud<pcl::PointXYZ>::Ptr
    copyAndFilterCluster
    ( pcl::PointCloud<pcl::PointXYZ>::Ptr working_cluster);

    Eigen::Vector4f
    computeClusterCentroid
    (pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster);


//struct mytype{
   // Eigen::Quaternionf bboxQuaternion;
  //  Eigen::Vector3f bboxTransform;
   // pcl::PointXYZ minPoint, maxPoint;
//};

 //mytype computeBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cluster, Eigen::Vector4f &cluster_centroid);



};


