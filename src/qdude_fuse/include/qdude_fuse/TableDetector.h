#include <QThread>
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>

#ifndef TableDetector_H
#define TableDetector_H

class  TableDetector : public QThread
{
public:
    struct TableGeometry {
        Eigen::Vector3f bboxTransform;
        Eigen::Quaternionf bboxQuaternion;
        pcl::PointXYZ minPoint;
        pcl::PointXYZ maxPoint;
    } DetectedTableGeometry;

    void run();

    TableDetector();
    pcl::PointCloud<pcl::PointXYZ>::Ptr convertPC2toPxyz (const sensor_msgs::PointCloud2 &input);

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformStartCloud (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered);

    pcl::PointCloud<pcl::PointXYZ>::Ptr passThroughZ (pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud);

    pcl::PointCloud<pcl::PointXYZ>::Ptr passThroughY (pcl::PointCloud<pcl::PointXYZ>::Ptr zrange_filteredCloud);

    pcl::PointCloud<pcl::PointXYZ>::Ptr planeSegmentation (pcl::PointCloud<pcl::PointXYZ>::Ptr yzrange_filteredCloud);

    void normalEstimationAndClustering
    (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,
     pcl::PointCloud<pcl::Normal>::Ptr normals,
     std::vector <pcl::PointIndices> &clusters);

    void computeClusterCentroid
    (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,
     Eigen::Vector4f &cluster_centroid);

    void computeBoundingBox
    (pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p,
     Eigen::Vector4f &cluster_centroid,
     Eigen::Vector3f &bboxTransform,
     Eigen::Quaternionf &bboxQuaternion,
     pcl::PointXYZ &minPoint, pcl::PointXYZ &maxPoint);

    void cloudCB(const sensor_msgs::PointCloud2 &input);
    void cloudCB2(const sensor_msgs::PointCloud2 &input);
    void timerCB(const ros::TimerEvent&);

protected:
    ros::NodeHandle nh;
    ros::Subscriber pcl_sub;
    ros::Publisher pcl_pub, ind_pub, coef_pub;
    pcl::visualization::PCLVisualizer viewer; //CloudViewer viewer;
    ros::Timer viewer_timer;

};

#endif
