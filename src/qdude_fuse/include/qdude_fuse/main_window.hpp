/**
 * @file /include/qdude/main_window.hpp
 *
 * @brief Qt based gui for qdude.
 *
 * @date November 2010
 **/
#ifndef qdude_MAIN_WINDOW_H
#define qdude_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui/QMainWindow>
#include "ui_main_window.h"
#include "qnode.hpp"
#include "../include/qdude_fuse/test.hpp"
#include "../include/qdude_fuse/TableDetector.h"

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace qdude {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void closeEvent(QCloseEvent *event); // Overloaded function
	void showNoMasterMessage();


public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
    void on_button_connect_clicked(bool check );
    void on_pushButton_clicked();
    void on_BttnStartTableDetection_clicked();
    void on_BttnStopTableDetection_clicked();

private:
    Ui::MainWindowDesign ui;
	QNode qnode;

    /* Store a global obect instance to store data */
    PointcloudProcessing pcp;
    TableDetector *detector;

};

}  // namespace qdude

#endif // qdude_MAIN_WINDOW_H
