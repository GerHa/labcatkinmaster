#include <pcl/io/pcd_io.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/PointCloud2.h>

int
main(int argc, char** argv)
{
    // Object for storing the point cloud, with color information.
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::PCLPointCloud2 cloud_blob;
    pcl::io::loadPCDFile("/home/gerry/Bilder/1447065094.372686184.pcd", cloud_blob);

pcl::fromPCLPointCloud2 (cloud_blob, *cloud); //* convert from pcl/PCLPointCloud2 to pcl::PointCloud<T>

    for (size_t i = 0; i < cloud->points.size (); ++i)
        std::cout << "    " << cloud->points[i].x
                  << " "    << cloud->points[i].y
                  << " "    << cloud->points[i].z
                  << " "    << cloud->points[i].r << std::endl;

//    // Object for storing the point cloud, with color information.
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

//    // Read a PCD file from disk.
//    if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(argv[1], *cloud) != 0)
//    {
//        return -1;
//    }

//    // kd-tree object for searches.
//    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZRGB>);
//    kdtree->setInputCloud(cloud);

//    // Color-based region growing clustering object.
//       pcl::RegionGrowingRGB<pcl::PointXYZRGB> clustering;
//    clustering.setInputCloud(cloud);
//    clustering.setSearchMethod(kdtree);
//    // Here, the minimum cluster size affects also the postprocessing step:
//    // clusters smaller than this will be merged with their neighbors.
//    clustering.setMinClusterSize(100);
//    // Set the distance threshold, to know which points will be considered neighbors.
//    clustering.setDistanceThreshold(10);
//    // Color threshold for comparing the RGB color of two points.
//   clustering.setPointColorThreshold(6);
//    // Region color threshold for the postprocessing step: clusters with colors
//   // within the threshold will be merged in one.
//   clustering.setRegionColorThreshold(5);

//    std::vector <pcl::PointIndices> clusters;
//    clustering.extract(clusters);

//    // For every cluster...
//    int currentClusterNum = 1;
//    for (std::vector<pcl::PointIndices>::const_iterator i = clusters.begin(); i != clusters.end(); ++i)
//    {
//        // ...add all its points to a new cloud...
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cluster(new pcl::PointCloud<pcl::PointXYZRGB>);
//        for (std::vector<int>::const_iterator point = i->indices.begin(); point != i->indices.end(); point++)
//            cluster->points.push_back(cloud->points[*point]);
//        cluster->width = cluster->points.size();
//        cluster->height = 1;
//        cluster->is_dense = true;

//        // ...and save it to disk.
//        if (cluster->points.size() <= 0)
//            break;
//        std::cout << "Cluster " << currentClusterNum << " has " << cluster->points.size() << " points." << std::endl;
//        std::string fileName = "cluster" + boost::to_string(currentClusterNum) + ".pcd";
//        pcl::io::savePCDFileASCII(fileName, *cluster);

//        currentClusterNum++;
 //   }
}
