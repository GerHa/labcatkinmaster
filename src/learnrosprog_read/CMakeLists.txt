cmake_minimum_required(VERSION 2.8.3)
project(learnrosprog_read)


find_package(catkin REQUIRED COMPONENTS
  pcl_conversions
  pcl_msgs
  pcl_ros
  sensor_msgs
)
find_package(PCL REQUIRED)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES learnrosprog_read
#  CATKIN_DEPENDS pcl_conversions pcl_msgs pcl_ros sensor_msgs
#  DEPENDS system_lib
)


file(GLOB_RECURSE QT_MOC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS include/header/*.hpp)

include_directories(${catkin_INCLUDE_DIRS})
include_directories(include ${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})

add_executable(pcl_createpc src/pcl_createpc.cpp)
target_link_libraries(pcl_createpc ${catkin_LIBRARIES} ${PCL_LIBRARIES})

#add_executable(cloudhandle src/cloudhandle.cpp)
#target_link_libraries(cloudhandle ${catkin_LIBRARIES} ${PCL_LIBRARIES})
# add_executable(learnrosprog_node src/learnrosprog_node.cpp)


# add_dependencies(learnrosprog_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})


# target_link_libraries(learnrosprog_node
#   ${catkin_LIBRARIES}
# )

