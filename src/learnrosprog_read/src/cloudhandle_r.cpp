
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <pcl/visualization/cloud_viewer.h>
#include "cloudhandle_r.h"

cloudHandler::cloudHandler()
: viewer("Cloud Viewer")
{
    pcl_sub = nh.subscribe("pcl_output", 10, &cloudHandler::cloudCB, this);
    viewer_timer = nh.createTimer(ros::Duration(0.1), &cloudHandler::timerCB, this);
}

void cloudHandler::cloudCB(const sensor_msgs::PointCloud2 &input)
{
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::fromROSMsg(input, cloud);

    viewer.showCloud(cloud.makeShared());
}

void cloudHandler::timerCB(const ros::TimerEvent&)
{
    if (viewer.wasStopped())
    {
        ros::shutdown();
    }
}
