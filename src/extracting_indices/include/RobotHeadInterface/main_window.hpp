/**
 * @file /include/RobotHeadInterface/main_window.hpp
 *
 * @brief Qt based gui for RobotHeadInterface.
 *
 * @date November 2010
 **/
#ifndef RobotHeadInterface_MAIN_WINDOW_H
#define RobotHeadInterface_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <iostream>
#include "ui_main_window.h"
#include "qnode.hpp"

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace RobotHeadInterface {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/

class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void ReadSettings(); // Load up qt program settings at startup
	void WriteSettings(); // Save qt program settings when closing

	void closeEvent(QCloseEvent *event); // Overloaded function
    void showNoMasterMessage();

public Q_SLOTS:
    void on_button_connect_clicked(bool check );

Q_SIGNALS:
 void rosShutdown();

private:
	Ui::MainWindowDesign ui;
	QNode qnode;
    QTimer *timer;
};

}  // namespace RobotHeadInterface

#endif // RobotHeadInterface_MAIN_WINDOW_H
