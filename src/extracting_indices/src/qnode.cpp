/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <QMessageBox>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include "../include/RobotHeadInterface/qnode.hpp"

#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>




/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace RobotHeadInterface {

/*****************************************************************************
** Implementation
*****************************************************************************/

QNode::QNode(int argc, char** argv ) :               // Constructor for QNode
    init_argc(argc),
    init_argv(argv)
{}

QNode::~QNode() {                                    // Destructor for QNode
    if(ros::isStarted()) {

        // explicitly needed since we use ros::start();
        ros::shutdown();
        ros::waitForShutdown();
    }
    wait();
}
// Function initialize of QNode when called with no input
// This function starts and creates a Node.
// The types of the messages are defined in this function
bool QNode::init() {

    // initialize ros with no specific input arguments and name 'RobotHeadInterface'
    ros::init(init_argc,init_argv,"RobotHeadInterface");
    //If ros::master not online, initialize is false
    if ( ! ros::master::check() ) {
        return false;
    }

    // explicitly needed since our nodehandle is going out of scope
    ros::start();
    // Define nh as NodeHandle.
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2), cloud_filtered_blob (new pcl::PCLPointCloud2);
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

     // Fill in the cloud data
     pcl::PCDReader reader;
     reader.read ("/home/gerry/catkin_ws/src/extracting_indices/table_scene_lms400.pcd", *cloud_blob);

     std::cerr << "PointCloud before filtering: " << cloud_blob->width * cloud_blob->height << " data points." << std::endl;

     // Create the filtering object: downsample the dataset using a leaf size of 1cm
     pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
     sor.setInputCloud (cloud_blob);
     sor.setLeafSize (0.01f, 0.01f, 0.01f);
     sor.filter (*cloud_filtered_blob);

     // Convert to the templated PointCloud
     pcl::fromPCLPointCloud2 (*cloud_filtered_blob, *cloud_filtered);

     std::cerr << "PointCloud after filtering: " << cloud_filtered->width * cloud_filtered->height << " data points." << std::endl;

     // Write the downsampled version to disk
     pcl::PCDWriter writer;
     writer.write<pcl::PointXYZ> ("table_scene_lms400_downsampled.pcd", *cloud_filtered, false);

     pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
     pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
     // Create the segmentation object
     pcl::SACSegmentation<pcl::PointXYZ> seg;
     // Optional
     seg.setOptimizeCoefficients (true);
     // Mandatory
     seg.setModelType (pcl::SACMODEL_PLANE);
     seg.setMethodType (pcl::SAC_RANSAC);
     seg.setMaxIterations (1000);
     seg.setDistanceThreshold (0.01);

     // Create the filtering object
     pcl::ExtractIndices<pcl::PointXYZ> extract;

     int i = 0, nr_points = (int) cloud_filtered->points.size ();
     // While 30% of the original cloud is still there
     while (cloud_filtered->points.size () > 0.3 * nr_points)
     {
       // Segment the largest planar component from the remaining cloud
       seg.setInputCloud (cloud_filtered);
       seg.segment (*inliers, *coefficients);
       if (inliers->indices.size () == 0)
       {
         std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
         break;
       }

       // Extract the inliers
       extract.setInputCloud (cloud_filtered);
       extract.setIndices (inliers);
       extract.setNegative (false);
       extract.filter (*cloud_p);
       std::cerr << "PointCloud representing the planar component: " << cloud_p->width * cloud_p->height << " data points." << std::endl;

       std::stringstream ss;
       ss << "table_scene_lms400_plane_" << i << ".pcd";
       writer.write<pcl::PointXYZ> (ss.str (), *cloud_p, false);

       // Create the filtering object
       extract.setNegative (true);
       extract.filter (*cloud_f);
       cloud_filtered.swap (cloud_f);
       i++;
     }

     return (0);
   

    ros::spin();
    // Init was sucessfull
    return true;
}

// Function run() of QNode.
void QNode::run() {
    ros::Rate loop_rate(1000);

    while ( ros::ok() ) {
    }
    // used to signal the gui for a shutdown (useful to roslaunch)
    Q_EMIT rosShutdown();
}

void QNode::cloud_cb(const sensor_msgs::PointCloud2ConstPtr& input)
{
    // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud
     pcl::PointCloud<pcl::PointXYZ> cloud;
     pcl::fromROSMsg (*input, cloud);

     pcl::ModelCoefficients coefficients;
     pcl::PointIndices inliers;

     // Create the segmentation object
     pcl::SACSegmentation<pcl::PointXYZ> seg;
     // Optional
     seg.setOptimizeCoefficients (true);
     // Mandatory
     seg.setModelType (pcl::SACMODEL_PLANE);
     seg.setMethodType (pcl::SAC_RANSAC);
     seg.setDistanceThreshold (0.01);

     seg.setInputCloud (cloud.makeShared ());
     seg.segment (inliers, coefficients);

     // Publish the model coefficients
     pcl_msgs::ModelCoefficients ros_coefficients;
     pcl_conversions::fromPCL(coefficients, ros_coefficients);
     pub.publish (ros_coefficients);


}
}  // namespace RobotHeadInterface
