/**
 * @file /include/qdude/main_window.hpp
 *
 * @brief Qt based gui for qdude.
 *
 * @date November 2010
 **/
#ifndef qdude_MAIN_WINDOW_H
#define qdude_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui/QMainWindow>
#include "ui_main_window.h"
#include "qnode.hpp"

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace qdude {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void closeEvent(QCloseEvent *event); // Overloaded function
	void showNoMasterMessage();

public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
	void on_button_connect_clicked(bool check );

private:
	Ui::MainWindowDesign ui;
	QNode qnode;
};

}  // namespace qdude

#endif // qdude_MAIN_WINDOW_H
